from concurrent import futures
from LogicLayer.LogicAPI import LogicAPI
import grpc
import LogicLayer_pb2 
import LogicLayer_pb2_grpc

class Greeter(LogicLayer_pb2_grpc.GreeterServicer):
    """ 
    The Greeter class is the class which takes in input from the client side. In other words, the main server.
    It uses gRPC to take in information and relay it back to the client.

    The Greeter takes any data that it receives and sends it to the appropriate LogicAPI function. The Greeter
    does not perform any logic itself, it simply relays data.
   
    Methods:
        authorize(),
        buy_ticket(),
        check_duration(),
        check_weather(),
        create_bus_stop(),
        create_user(),
        delete_bus_stop(),
        delete_user(),
        favorite_bus_stop(),
        get_favorite_bus_stops(),
        get_user(),
        gift_ticket(),
        read_all_bus_stops(),
        read_all_users(),
        read_ticket(),
        unfavorite_bus_stop(),
        update_bus_stop(),
        update_user(),
        use_ticket().
    """
    def __init__(self) -> None:
        """ 
        Initializes the Greeter class.
        """
        self.logic = LogicAPI()
   
    def create_user(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to create_user
        in the LogicAPI.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.create_user(request.name, request.password, request.birthday, request.phone, request.isadmin)
        return LogicLayer_pb2.created_user(message=message)

    def read_all_users(self, request, context):
        """ 
        This function takes in the request from the client side and calls on the read_all_users function 
        in the LogicAPI.

        It returns an error message back to the client alongside a stringified dictionary of all users.
        """        
        allusers = self.logic.read_all_users()
        return LogicLayer_pb2.all_users(message=allusers)

    def authorize(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the authorize
        function in the LogicAPI,

        It returns a stringified JSON message which holds the status code and status message of the call 
        as well as the the users name as an additional parameter.
        """
        message = self.logic.authorize(request.name, request.password)
        return LogicLayer_pb2.authorize_message(message=message)    

    def update_user(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to update a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.update_user(request.old_name, request.old_password, request.keyword, request.changed_var, request.isadmin)
        return LogicLayer_pb2.updated_user(message=message)

    def delete_user(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to delete a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.delete_user(request.name, request.password, request.isadmin)
        return LogicLayer_pb2.deleted_user(message=message)

    def get_user(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to get all details of a user.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as all the user details of the specified user.
        """
        message = self.logic.get_user(request.name)
        return LogicLayer_pb2.user(message=message)

    def create_bus_stop(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to create_bus_stop
        in the LogicAPI.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """ 
        message = self.logic.create_bus_stop(request.name, request.latitude, request.longitude)
        return LogicLayer_pb2.created_bus_stop(message=message)

    def read_all_bus_stops(self, request, context):
        """ 
        This function takes in the request from the client side and relays the request to the LogicAPI bby
        calling the read_all_bus_stops function.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as a dictionary of all bus stops as an additional parameter.
        """
        message = self.logic.read_all_bus_stops()
        return LogicLayer_pb2.all_bus_stops(message=message)

    def update_bus_stop(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the update_bus_stop
        function in the LogicAPI.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.update_bus_stop(request.name, request.keyword, request.changed_var)
        return LogicLayer_pb2.updated_bus_stop(message=message)

    def delete_bus_stop(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the delete_bus_stop
        function in the LogicAPI.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.delete_bus_stop(request.name)
        return LogicLayer_pb2.deleted_bus_stop(message=message)

    def favorite_bus_stop(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI
        to favorite a bus stop for a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.favorite_bus_stop(request.user, request.name, "")
        return LogicLayer_pb2.favorite_bus_stops(message=message)
    
    def unfavorite_bus_stop(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI
        to unfavorite a bus stop for a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.unfavorite_bus_stop(request.user, request.name)
        return LogicLayer_pb2.unfavorited_bus_stop(message=message)
    
    def get_favorite_bus_stops(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI
        to get all the favorite bus stops for a user.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as a dictionary of all the favorited bus stops of the user as an additional parameter.
        """
        message = self.logic.get_favorite_bus_stops(request.user)
        return LogicLayer_pb2.gotten_favorite_bus_stops(message=message)

    def check_weather(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI
        to check the weather of a bus stop.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as a dicitonary with several details about the weather conditions of the bus stop as an additional parameter.
        """
        message = self.logic.get_current_weather_at_location(request.name)
        return LogicLayer_pb2.weather_answer(message=message)

    def check_duration(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI
        to check the duration of a commute between two bus stops.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as the minutes of time it takes to go between those two bus stops as an additional parameter.
        """
        message = self.logic.get_route_duration(request.origin, request.destination)
        return LogicLayer_pb2.duration_answer(message=message)

    def buy_ticket(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to buy a ticket for a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.buy_ticket(request.name)
        return LogicLayer_pb2.ticket_bought(message=message)
    
    def use_ticket(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to buy a use a ticket for a user.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.use_ticket(request.name, request.ticket_id)
        return LogicLayer_pb2.ticket_activated(message=message)
    
    def gift_ticket(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to gift a ticket from one user to another.

        It returns a stringified JSON message which holds the status code and status message of the call.
        """
        message = self.logic.gift_ticket(request.ticket_id, request.owner, request.gifter)
        return LogicLayer_pb2.gifted_ticket(message=message)
    
    def read_ticket(self, request, context):
        """ 
        This function takes in the request from the client side and relays the necessary data to the LogicAPI file
        to return the ticket history of a user.

        It returns a stringified JSON message which holds the status code and status message of the call,
        as well as a dictionary of all of the users tickets. (Unused, Activated or Expired)
        """
        message = self.logic.read_all_tickets(request.name)
        return LogicLayer_pb2.ticket_bought(message=message)

def server():
    """ Initializes the gRPC server. """
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=2))
    LogicLayer_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
    server.add_insecure_port('[::]:50051')
    print("gRPC starting")
    server.start()
    server.wait_for_termination()
server()