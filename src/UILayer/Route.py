from Client import Client
import json

class Route(object):
    """
    This class is the UI class for any Route functions in the system.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        check_duration().
    """
    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    
    def check_duration(self, origin: str, destination: str) -> dict:
        """
        Fetches information from the gRPC client regarding the commute duration between two bus stops.

        Takes in the variables "origin" and "destination", both strings, to check the duration between those two bus stops. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.check_duration(origin, destination)
        message = json.loads(message)
        return message
