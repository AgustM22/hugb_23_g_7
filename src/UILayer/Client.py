import grpc
import LogicLayer_pb2
import LogicLayer_pb2_grpc

class Client(object):
   """ 
   The Client class is the class which contacts the server side. It uses gRPC to contact the initiated server
   and returns the data it receives.

   Methods:
      authorize(),
      buy_ticket(),
      check_duration(),
      check_weather(),
      create_bus_stop(),
      create_user(), 
      delete_bus_stop(),
      delete_user(),
      favorite_bus_stop(),
      get_favorite_bus_stops(),
      get_user(),
      gift_ticket(),
      read_all_bus_stops(),
      read_all_users(),
      read_ticket(),
      unfavorite_bus_stop(),
      update_bus_stop(),
      update_user(),
      use_ticket().
   """

   def __init__(self) -> None:
      """ 
      Initializes the Client class.
      """
      pass

   def create_user(self, name: str, password: str, birthday: str, phone: str, isadmin: str = "0") -> str:
      """ 
      This function contacts the server side to create a user.

      It takes in the variables name, password, birthday, phone and isadmin. All of which should be strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call.
      """
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.create_user(LogicLayer_pb2.create_user_input(name=name, password=password, birthday=birthday, phone=phone, isadmin=isadmin))
         return response.message
   
   def read_all_users(self, noninput: str = "") -> str:
      """ 
      This function contacts the server side to get a dictionary of all users.

      It has the variable noninput, but as it's name suggests, it does nothing, so feel free to ignore it.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """      
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.read_all_users(LogicLayer_pb2.read_all_users_noninput(message=noninput))
         return response.message

   def authorize(self, name: str, password: str) -> str:
      """ 
      This function contacts the server side to authorize a users login.

      It takes in the variables name and password, both of which should be strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters),
      and the users name and whether they are an admin or not.
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.authorize(LogicLayer_pb2.authorize_input(name=name, password=password))
         return response.message

   def update_user(self, old_name: str, old_password: str, keyword: str, changed_var: str, isadmin: bool) -> str:
      """ 
      This function contacts the server side to update a user.

      It takes in the variables old_name, old_password, keyword, changed_var and isadmin. All of which should be strings, except isadmin which is a boolean.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.update_user(LogicLayer_pb2.update_user_input(old_name=old_name, old_password=old_password, keyword=keyword, changed_var=changed_var, isadmin=str(isadmin)))
         return response.message

   def delete_user(self, name: str, password: str, isadmin: bool) -> str:
      """ 
      This function contacts the server side to delete a user.

      It takes in the variables name, password and isadmin. Name and password are strings, while isadmin is a boolean.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.delete_user(LogicLayer_pb2.delete_user_input(name=name, password=password, isadmin=str(isadmin)))
         return response.message

   def get_user(self, name: str) -> str:
      """ 
      This function contacts the server side to create a new bus stop.

      It takes in the variables name, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.get_user(LogicLayer_pb2.get_user_input(name=name))
         return response.message

   def create_bus_stop(self, name: str, latitude: str, longitude: str) -> str:
      """ 
      This function contacts the server side to create a new bus stop.

      It takes in the variables name, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.create_bus_stop(LogicLayer_pb2.create_bus_stop_input(name=name, latitude=latitude, longitude=longitude))
         return response.message
   
   def read_all_bus_stops(self, noninput: str = "") -> str:
      """ 
      This function contacts the server side to get a stringified dictionary of all bus stops.

      It has the variable noninput, but as it's name suggests, it does nothing, so feel free to ignore it.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.read_all_bus_stops(LogicLayer_pb2.read_all_bus_stops_noninput(message=noninput))
         return response.message
   
   def update_bus_stop(self, name: str, keyword: str, changed_var: str) -> str:
      """ 
      This function contacts the server side to update a bus stop.

      It takes in the variables oldname and newname, both are strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.update_bus_stop(LogicLayer_pb2.update_bus_stop_input(name=name, keyword=keyword, changed_var=changed_var))
         return response.message
   
   def delete_bus_stop(self, name: str) -> str:
      """ 
      This function contacts the server side to delete a bus stop.

      It takes in the variable name, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.delete_bus_stop(LogicLayer_pb2.delete_bus_stop_input(name=name))
         return response.message
      
   def favorite_bus_stop(self, user: str, name: str) -> str:
      """ 
      This function contacts the server side to favorite a bus stop for a user.

      It takes in the variable name and user, which are both strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.favorite_bus_stop(LogicLayer_pb2.favorite_bus_stop_input(user=user, name=name))
         return response.message

   def unfavorite_bus_stop(self, user: str, name: str) -> str:
      """ 
      This function contacts the server side to unfavorite a bus stop for a user.

      It takes in the variable name and user, which are both strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.unfavorite_bus_stop(LogicLayer_pb2.unfavorite_bus_stop_input(user=user, name=name))
         return response.message

   def get_favorite_bus_stops(self, user: str) -> str:
      """ 
      This function contacts the server side to get the favorite bus stops of a user.

      It takes in the variable user, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters),
      and the favorite bus stops of the user.
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.get_favorite_bus_stops(LogicLayer_pb2.get_favorite_bus_stops_input(user=user))
         return response.message
      
   def check_weather(self, name: str) -> str:
      """ 
      This function contacts the server side to check the weather conditions of a bus stop.

      It takes in the variable name, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters),
      and additionally the weather conditions of the bus stop.
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.check_weather(LogicLayer_pb2.check_weather_input(name=name))
         return response.message
      
   def check_duration(self, origin: str, destination: str) -> str:
      """ 
      This function contacts the server side to check the route duration between two bus stops.

      It takes in the variable origin and destination, which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters),
      and additionally the commute duration between the two bus stops.
      """     
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.check_duration(LogicLayer_pb2.check_duration_input(origin=origin, destination=destination))
         return response.message
      
   def buy_ticket(self, name: str) -> str:
      """ 
      This function contacts the server side to buy a ticket for a user.

      It takes in the variable name which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """  
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.buy_ticket(LogicLayer_pb2.buy_ticket_input(name=name))
         return response.message

   def use_ticket(self, name: str, ticket_id: str) -> str:
      """ 
      This function contacts the server side to activate a ticket for a user.

      It takes in the variables name and ticket_id, which are both strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """    
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.use_ticket(LogicLayer_pb2.use_ticket_input(name=name, ticket_id=ticket_id))
         return response.message
   
   def gift_ticket(self, ticket_id: str, owner: str, gifter: str) -> str:
      """ 
      This function contacts the server side to gift a ticket from one user to another.

      It takes in the variables ticket_id, owner and gifter, all of which are strings.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters).
      """  
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.gift_ticket(LogicLayer_pb2.gift_ticket_input(ticket_id=ticket_id, owner=owner, gifter=gifter))
         return response.message

   def read_ticket(self, name: str) -> str:
      """ 
      This function contacts the server side to get the ticket history of a user.

      It takes in the variable name which is a string.
      Returns a stringified JSON dictionary from the server with relevant information from the call (status, parameters),
      and additionally the ticket history of the given user.
      """  
      with grpc.insecure_channel('localhost:50051') as channel:
         stub = LogicLayer_pb2_grpc.GreeterStub(channel)
         response = stub.read_ticket(LogicLayer_pb2.read_ticket_input(name=name))
         return response.message


if __name__ == '__main__':
   print(Client().check_duration('Mjódd', 'Samgöngustofa'))