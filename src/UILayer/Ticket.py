from Client import Client
import json

class Ticket(object):
    """
    This class is the UI class for any Ticket functions in the system.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        buy_ticket(),
        gift_ticket(),
        read_all_tickets(),
        use_ticket().
    """

    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    
    def buy_ticket(self, name: str) -> dict:
        """
        Sends information to the gRPC client to buy a ticket for a user.

        Takes in the variable "name" which is a string. "name" is the name of the user.
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.buy_ticket(name)
        message = json.loads(message)
        return message
    
    def use_ticket(self, name: str, ticket_id: str) -> dict:
        """
        Sends information to the gRPC client to activate a ticket for a user.

        Takes in the variables "name" and "ticket_id", both of which are strings. 
        "name" is the name of the user and "ticket_id" is the ID of the ticket to be activated.
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.use_ticket(name, ticket_id)
        message = json.loads(message)
        return message
    
    def gift_ticket(self, ticket_id: str, owner: str, gifter: str) -> dict:
        """
        Sends information to the gRPC client to gift a ticket from one user to another.

        Takes in the variables "ticket_id", "owner" and "gifter", all of which are strings. 
        "ticket_id" is the ID of the ticket to be gifted, "owner" is the user which holds the ticket and "gifter" is the user which will receive the ticket.
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.gift_ticket(ticket_id, owner, gifter)
        message = json.loads(message)
        return message
    
    def read_all_tickets(self, name: str) -> dict:
        """
        Sends information to the gRPC client to get the ticket history of a user.

        Takes in the variable "name" which is a string. "name" is the name of the user.
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.read_ticket(name)
        message = json.loads(message)
        return message