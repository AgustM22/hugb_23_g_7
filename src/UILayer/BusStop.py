from Client import Client
import json

class BusStop(object):
    """
    This BusStop class is used by the UIAPI class to perform actions related to the CRUD of Bus stops.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        create_bus_stop(),
        read_bus_stops(),
        update_bus_stop(),
        delete_bus_stop()
    """

    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    
    def create_bus_stop(self, name: str, latitude: str, longitude: str) -> dict:
        """
        Sends information to the gRPC client to create a bus stop.

        Takes in the variables "name", "latitude" and "longitude" which are all strings. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.create_bus_stop(name, latitude, longitude)
        message = json.loads(message)
        return message
    
    def read_bus_stops(self) -> dict:
        """
        Fetches information from the gRPC client to read all bus stops.

        Takes in no variables and returns a status and parameter dictionary from the server.
        """
        message = self.client.read_all_bus_stops()
        message = json.loads(message)
        return message

    def update_bus_stop(self, name: str, keyword: str, changed_var: str) -> dict:
        """
        Sends information to the gRPC client to update a bus stop.

        Takes in the variables "name", "keyword" and "changed_var" which are all strings. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.update_bus_stop(name, keyword, changed_var)
        message = json.loads(message)
        return message

    def delete_bus_stop(self, name: str) -> dict:
        """
        Sends information to the gRPC client to delete a bus stop.

        Takes in the variable "name" which is a string. 
        Returns a status and parameter dictionary from the server.
        """
        message =  self.client.delete_bus_stop(name)
        message = json.loads(message)
        return message