import json
from Client import Client
import datetime

class User(object):
    """
    This User class is used by the UIAPI class to perform actions related to the CRUD of users.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        create_account(),
        read_all_users(),
        authorize(),
        update_account(),
        delete_user(),
        get_user()
    """

    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    

    def create_account(self, name: str, password: str, birthday: str, phone: str, isadmin: str = "0") -> dict:
        """ 
        This function allows the user to create a new account.
        Takes several variables, all of which are strings, and then sends that data to the 
        Client to confirm with the server and create the account.

        Returns a status and parameter dictionary from the server.
        """
        message = self.client.create_user(name, password, birthday, phone, isadmin)
        message = json.loads(message)
        return message


    def read_all_users(self) -> dict:
        """
        This function contacts the server to read all users in the system.

        Takes in no variables.         
        Returns a status and parameter dictionary from the server with all the users of the system.
        """
        message = self.client.read_all_users()
        message = json.loads(message)
        return message


    def authorize(self, name: str, password: str) -> dict:
        """
        This function contacts the server with a name and a password to authorize whether they are a user in the system.

        Takes in the variables "name" and "password", both strings and returns a status and parameter dictionary from the server.
        """
        message = self.client.authorize(name, password)
        message = json.loads(message)
        return message


    def update_account(self, old_name: str, old_password: str, keyword: str, changed_var: str, isadmin: bool) -> dict:
        """ 
        This function allows a user to update an account.
        Takes several variables, all of which are strings except isadmin which is a boolean, and then sends that data to the 
        Client to confirm with the server and update the accounr.

        Returns a status and parameter dictionary from the server.
        """
        message = self.client.update_user(old_name, old_password, keyword, changed_var, isadmin)
        message = json.loads(message)
        return message
    

    def delete_user(self, name: str, password: str, isadmin: bool) -> dict:
        """
        This function allows the user to delete an account.
        Takes "name", "password" and "isadmin" (isadmin is a boolean while rest are strings) and then sends that data to the 
        Client to confirm with the server and delete the account.

        Returns a status and parameter dictionary from the server.
        """
        message = self.client.delete_user(name, password, isadmin)
        message = json.loads(message)
        return message


    def get_user(self, name: str):
        """
        This function allows the user to read the information of their account.
        
        Calls the Client and returns a status and parameter dictionary from the server.
        """
        message = self.client.get_user(name)
        message = json.loads(message)
        return message