from tkinter import Label, Entry, Button, mainloop, W, Tk
from tkinter import messagebox as mb
from User import User
from BusStop import BusStop
from FavoriteBusStop import FavoriteBusStop
from Weather import Weather
from Route import Route
from Ticket import Ticket

class UIAPI(object):
    """ 
    The UIAPI class is the main UILayer file it creates a Tkinter UI interface for the user to interact with,
    but generally does not provide any "logic" on it's own. It simply calls on other UI files to contact the
    gRPC client/server with the data given from the user, then shows different things depending on the output.

    The class has a lot of methods, but it is more accurate to think of each method as a "Screen" and generally,
    no function should be called except by the appropriate function.

    Methods:
        __add_favorite_bus_stop(), 
        __admin_print_tickets(), 
        __authorize(), 
        __back(), 
        __buy_ticket(), 
        __check_route_duration(), 
        __check_weather_on_bus_stop(), 
        __confirm_deletion_bus_stop(), 
        __confirm_deletion_user(), 
        __create_account(), 
        __create_bus_stop(), 
        __delete_bus_stop(), 
        __delete_favorite_bus_stop(), 
        __delete_user(), 
        __end(), 
        __get_ticket_dict(), 
        __gift_ticket(),
        __login(),
        __menu(),
        __move_to_login(),
        __pass_bus_stop(),
        __pass_bus_update(),
        __pass_buy_ticket(),
        __pass_delete_favorite_bus_stop(),
        __pass_duration(),
        __pass_favorite_bus_stop(),
        __pass_gift_ticket(),
        __pass_update(),
        __pass_use_ticket(),
        __pass_user_favorite_bus_stop(),
        __pass_weather(),
        __print_tickets(),
        __printerror(),
        __read_all_bus_stops(),
        __read_favorite_bus_stops(),
        __read_users(),
        __sign_up(),
        __start(),
        __ticket_history(),
        __update_account(),
        __update_account_keyword(),
        __update_bus_stop(),
        __update_bus_stop_keyword(),
        __use_ticket(),
        __wipe_board(),
    """
    
    def __init__(self) -> None:
        """ 
        Initializing base variables then calls the start of the GUI.
        """

        # So you want to learn or work on this file, eh? 
        # Well if you want to survive out there you're gonna have to learn some things!

        # First of all, self.locationdict. 
        # That variable is by far on of the most important in this file, so you better understand it.
        # The dictionary holds onto every Label, Entry and Button in the tkinter window in a very specific way,
        # Each element gets a "Tag" such as "L0", "E6", "BB", etc.
        # This tag is made out of a Letter (L for label, E for entry, B for button) then a number (Or, very rarely another letter).
        # These tags can be called upon to get any info out of any tag, and is used by some important functions such as __wipe_board.
        # __wipe_board, whenever called, will always wipe out any label, entry or button that has a number whenever a screen is changed!
        # So, if you want a label or button to stick around name it "L" + another letter, 
        # and if it's supposed to dissapear on the next screen then make it numbered!

        # Second, Rows and Columns.
        # Whenever you define an element then keep in mind that the screen is centered on column 3, 
        # and you shouldn't place any element in column 0 or more than 30. (Think of those numbers as the borders)
        # This is a general rule of thumb to keep the screen consistent, remember it!
        
        # Finally, adding more functions to the main menu.
        # Whenever you wish to add another function to the main screen, then you simply add the name of the button
        # and the command to call it within the appropriate lists in the function __menu.
        # It should be pretty painless to add any function to either the admin or the user menu as long as you know what you're doing.

        # There is more, but I don't wanna sit here and tell you each thing, so go on then ya scurvy dog! Make me proud!

        self.master = Tk()
        self.user = User()
        self.busstop = BusStop()
        self.favbusstop = FavoriteBusStop()
        self.weather = Weather()
        self.route = Route()
        self.ticket = Ticket()

        self.stack = [] # Used for the "back" (self.__back) button
        self.SUCCESS = 200
        self.name = None
        self.admin = False
        self.locationdict = {}
        self.__start()


    def __start(self) -> None:
        """
        Prints the starting screen and allows the user to either log in or create an account.

        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.locationdict[f"LL"] = Label(self.master, font="TkFixedFont", text = r"""
                                                                                       
     ___  __    ___       ___  ________  ___  __    ___       ________  ________       
    |\  \|\  \ |\  \     |\  \|\   __  \|\  \|\  \ |\  \     |\   __  \|\   __  \      
    \ \  \/  /|\ \  \    \ \  \ \  \|\  \ \  \/  /|\ \  \    \ \  \|\  \ \  \|\  \     
     \ \   ___  \ \  \    \ \  \ \   ____\ \   ___  \ \  \    \ \  \\\  \ \   ____\    
      \ \  \\ \  \ \  \____\ \  \ \  \___|\ \  \\ \  \ \  \____\ \  \\\  \ \  \___|    
       \ \__\\ \__\ \_______\ \__\ \__\    \ \__\\ \__\ \_______\ \_______\ \__\       
        \|__| \|__|\|_______|\|__|\|__|     \|__| \|__|\|_______|\|_______|\|__|       
                                                                                       
""").grid(row = 0, column = 0, pady = 2, padx = 2, columnspan=4)
        self.locationdict[f"LM"] = Label(self.master, text = "Welcome to KlipKlop, please log in or register to continue.").grid(row = 1, column = 0, columnspan=4, pady = 2, padx = 2)
        self.locationdict[f"B1"] = Button(self.master, command=self.__login, text="Login")
        self.locationdict[f"B1"].grid(row = 2, column = 1, pady=10, padx=10)
        self.locationdict[f"B2"] = Button(self.master, command=self.__sign_up, text="Create Account")
        self.locationdict[f"B2"].grid(row = 2, column = 2, pady=10, padx=10)

        mainloop()


    def __authorize(self) -> None:
        """
        Authorizes the users login and re-sets several variables for the GUI.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        name = self.locationdict[f"E1"].get()
        password = self.locationdict[f"E2"].get()
        message = self.user.authorize(name, password)
        if message["code"] == self.SUCCESS:
            self.name = message["parameters"][0]
            self.admin = message["parameters"][1]
            self.master.destroy()
            self.locationdict = {}
            self.master = Tk()
            self.__menu()
        else:
            self.__printerror(message["message"])


    def __move_to_login(self) -> None:
        """
        Takes the user from signing up to logging in.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        name = self.locationdict["E1"].get()
        password = self.locationdict["E2"].get()
        birthday = self.locationdict["E3"].get()
        phonenumber = self.locationdict["E4"].get()
        message = self.user.create_account(name, password, birthday, phonenumber)
        if message["code"] == self.SUCCESS:
            self.__wipe_board()
            if self.name == None:
                self.__login()
            else:
                self.__back()
        else:
            self.__printerror(message["message"])


    def __login(self) -> None:
        """
        Creates the login screen for the user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()

        self.locationdict[f"L1"] = Label(self.master, text = "Name:").grid(row = 2, column = 1, pady = 2, padx = 10)
        self.locationdict[f"E1"] = Entry(self.master)
        self.locationdict[f"E1"].grid(row = 2, column = 2, pady = 2, padx = 2)
        self.locationdict[f"L2"] = Label(self.master, text = "Password:").grid(row = 3, column = 1, pady = 2, padx = 10)
        self.locationdict[f"E2"] = Entry(self.master)
        self.locationdict[f"E2"].grid(row = 3, column = 2, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__authorize, text="Login")
        self.locationdict[f"B1"].grid(row = 4, column = 1, columnspan=2, pady=10, padx=10)


    def __sign_up(self) -> None:
        """
        Creates the sign up screen for the user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L1"] = Label(self.master, text = "Please input the name of your profile")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L2"] = Label(self.master, text = "Please input your password. (7+ Letters, needs 2 digits.)")
        self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L3"] = Label(self.master, text = "What is your birthday? DD/MM/YYYY")
        self.locationdict[f"L3"].grid(row = 4, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L4"] = Label(self.master, text = "What is your phonenumber?:")
        self.locationdict[f"L4"].grid(row = 5, column = 1, pady = 2, padx = 10, sticky=W)

        for i in range(4):
            self.locationdict[f"E{i+1}"] = Entry(self.master)
            self.locationdict[f"E{i+1}"].grid(row = i+2, column = 3, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__move_to_login, text="Create Account")
        self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __create_account(self) -> None:
        """
        Allows an admin to create a user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L1"] = Label(self.master, text = "Please input the name of the profile")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L2"] = Label(self.master, text = "Please input the password. (7+ Letters, needs 2 digits.)")
        self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L3"] = Label(self.master, text = "What is the birthday of the user? DD/MM/YYYY")
        self.locationdict[f"L3"].grid(row = 4, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L4"] = Label(self.master, text = "What is the phonenumber of the user?:")
        self.locationdict[f"L4"].grid(row = 5, column = 1, pady = 2, padx = 10, sticky=W)

        for i in range(4):
            self.locationdict[f"E{i+1}"] = Entry(self.master)
            self.locationdict[f"E{i+1}"].grid(row = i+2, column = 4, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__move_to_login, text="Create Account")
        self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __menu(self) -> None:
        """
        Creates the main menu screen for both admin and a normal user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.locationdict[f"LL"] = Label(text = "KlipKlop")
        self.locationdict[f"LL"].grid(row=0, column=0, pady = 10, padx = 10)
        self.locationdict[f"LR"] = Label(text = "Group 7")
        self.locationdict[f"LR"].grid(row=0, column=20, pady = 10, padx = 10)
        self.locationdict[f"BB"] = Button(text = "Back", command=self.__back)
        self.locationdict[f"BB"].grid(row=30, column=3, pady = 10, padx = 10)
        if self.admin:
            admin_list = ["Create New User", "Read All Users", "Update User", 
                          "Delete User", "Create Bus Stop", "Read All Bus Stops", 
                          "Update Bus Stop", "Delete Bus Stop", "Check Weather on Bus Stop", 
                          "Check Route Duration", "Read all Favorite Bus Stops", 
                          "Add Favorite Bus Stop", "Delete Favorite Bus Stop", "Buy Ticket for User", 
                          "Activate a Ticket for a User", 
                          "Send a Ticket from a User to Another", "See Active & Used Tickets of a User",
                          "Close Program"]
            admin_command_list = [self.__create_account, self.__read_users, self.__update_account, self.__delete_user, 
                                  self.__create_bus_stop, self.__read_all_bus_stops, self.__update_bus_stop, self.__delete_bus_stop,
                                  self.__check_weather_on_bus_stop, self.__check_route_duration, self.__read_favorite_bus_stops, 
                                  self.__add_favorite_bus_stop, self.__delete_favorite_bus_stop, 
                                  self.__buy_ticket, self.__use_ticket, self.__gift_ticket, self.__ticket_history,
                                  self.__end]
            self.locationdict[f"L0"] = Label(text = f"Welcome Admin {self.name}. Please select an option.")
            self.locationdict[f"L0"].grid(row=1, column=3, pady = 10, padx = 10)
            for i in range(4):
                self.locationdict[f"B{i+1}"] = Button(text = admin_list[i], command = admin_command_list[i])
                self.locationdict[f"B{i+1}"].grid(row = i+2, column = 2, pady = 2, padx = 2, sticky="ew")
            for index, i in enumerate(range(4, 8)):
                self.locationdict[f"B{i+1}"] = Button(text = admin_list[i], command = admin_command_list[i])
                self.locationdict[f"B{i+1}"].grid(row = index+2, column = 4, pady = 2, padx = 2, sticky="ew")
            for index, i in enumerate(range(8, len(admin_list))):
                self.locationdict[f"B{i+1}"] = Button(text = admin_list[i], command = admin_command_list[i])
                self.locationdict[f"B{i+1}"].grid(row = index+2, column = 3, pady = 2, padx = 2, sticky="ew")
        else:
            commute_list = ["Update Profile", "Delete Profile", "Read All Bus Stops", 
                            "Check Weather on Bus Stop", "Check Route Duration", 
                            "Read all Favorite Bus Stops", "Add Favorite Bus Stop", 
                            "Delete Favorite Bus Stop", "Buy Ticket", "Use a Ticket", 
                            "Send a Ticket", "See Active & Used Tickets History",
                            "Close Program"]
            commute_command_list = [self.__update_account, self.__delete_user, self.__read_all_bus_stops, 
                                    self.__check_weather_on_bus_stop, self.__check_route_duration, self.__read_favorite_bus_stops, 
                                    self.__add_favorite_bus_stop, self.__delete_favorite_bus_stop, 
                                    self.__buy_ticket, self.__use_ticket, self.__gift_ticket, self.__ticket_history,
                                    self.__end]
            self.locationdict[f"L0"] = Label(text = f"Welcome User {self.name}. Please select an option.")
            self.locationdict[f"L0"].grid(row=1, column=3, pady = 10, padx = 10)
            for i in range(len(commute_list)):
                self.locationdict[f"B{i+1}"] = Button(text = commute_list[i], command = commute_command_list[i])
                self.locationdict[f"B{i+1}"].grid(row = i+2, column = 3, pady = 2, padx = 2, sticky="ew")


    def __read_users(self) -> None:
        """
        Creates a screen for an admin to read all users in the database.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)
        
        message = self.user.read_all_users()
        listOfUsers = message["parameters"]
        self.locationdict[f"L1"] = Label(text = "Name")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
        self.locationdict[f"L2"] = Label(text = "Password")
        self.locationdict[f"L2"].grid(row = 2, column = 2, pady = 2, padx = 2)
        self.locationdict[f"L3"] = Label(text = "Birthday")
        self.locationdict[f"L3"].grid(row = 2, column = 3, pady = 2, padx = 2)
        self.locationdict[f"L4"] = Label(text = "Phone")
        self.locationdict[f"L4"].grid(row = 2, column = 4, pady = 2, padx = 2)
        self.locationdict[f"L5"] = Label(text = "Admin")
        self.locationdict[f"L5"].grid(row = 2, column = 5, pady = 2, padx = 2)
        for index, elem in enumerate(listOfUsers):
            self.locationdict[f"L{(index+1)*6}"] = Label(text = elem["name"])
            self.locationdict[f"L{(index+1)*6}"].grid(row = index+3, column = 1, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*6+1}"] = Label(text = elem["password"])
            self.locationdict[f"L{(index+1)*6+1}"].grid(row = index+3, column = 2, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*6+2}"] = Label(text = elem["birthday"])
            self.locationdict[f"L{(index+1)*6+2}"].grid(row = index+3, column = 3, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*6+3}"] = Label(text = elem["phone"])
            self.locationdict[f"L{(index+1)*6+3}"].grid(row = index+3, column = 4, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*6+4}"] = Label(text = elem["isadmin"])
            self.locationdict[f"L{(index+1)*6+4}"].grid(row = index+3, column = 5, pady = 2, padx = 2)


    def __update_account(self) -> None:
        """
        Creates a screen for a normal user to choose what to update for their profile.
        If the user is an admin then it creates a screen for them to update any information for any profile.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)
        if self.admin:
            self.locationdict[f"L1"] = Label(text = "Please select what you'd like to change:")
            self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 2)
            self.locationdict["B1"] = Button(text = "Username", command= lambda: self.__update_account_keyword("username"))
            self.locationdict["B1"].grid(row = 3, column = 3, pady = 2, padx = 2)
            self.locationdict["B2"] = Button(text = "Password", command= lambda: self.__update_account_keyword("password"))
            self.locationdict["B2"].grid(row = 4, column = 3, pady = 2, padx = 2)
            self.locationdict["B3"] = Button(text = "Birthday", command= lambda: self.__update_account_keyword("birthday"))
            self.locationdict["B3"].grid(row = 5, column = 3, pady = 2, padx = 2)
            self.locationdict["B4"] = Button(text = "Phonenumber", command= lambda: self.__update_account_keyword("phone"))
            self.locationdict["B4"].grid(row = 6, column = 3, pady = 2, padx = 2)
            self.locationdict["B5"] = Button(text = "Admin Priveleges", command= lambda: self.__update_account_keyword("isadmin"))
            self.locationdict["B5"].grid(row = 7, column = 3, pady = 2, padx = 2)
        else:
            self.locationdict[f"L1"] = Label(text = "Please select what you'd like to change:")
            self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 2)

            message = self.user.get_user(self.name) # Getting all of the users info.
            user = message["parameters"]
            self.locationdict[f"L2"] = Label(text = "Name")
            self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 2)
            self.locationdict[f"L3"] = Label(text = "Password")
            self.locationdict[f"L3"].grid(row = 3, column = 2, pady = 2, padx = 2)
            self.locationdict[f"L4"] = Label(text = "Birthday")
            self.locationdict[f"L4"].grid(row = 3, column = 4, pady = 2, padx = 2)
            self.locationdict[f"L5"] = Label(text = "Phone")
            self.locationdict[f"L5"].grid(row = 3, column = 5, pady = 2, padx = 2)

            self.locationdict[f"L7"] = Label(text = user["name"])
            self.locationdict[f"L7"].grid(row = 4, column = 1, pady = 2, padx = 2)
            self.locationdict[f"L8"] = Label(text = user["password"])
            self.locationdict[f"L8"].grid(row = 4, column = 2, pady = 2, padx = 2)
            self.locationdict[f"L9"] = Label(text = user["birthday"])
            self.locationdict[f"L9"].grid(row = 4, column = 4, pady = 2, padx = 2)
            self.locationdict[f"L10"] = Label(text = user["phone"])
            self.locationdict[f"L10"].grid(row = 4, column = 5, pady = 2, padx = 2)

            self.locationdict["B1"] = Button(text = "Username", command= lambda: self.__update_account_keyword("username"))
            self.locationdict["B1"].grid(row = 3, column = 3, pady = 2, padx = 2)
            self.locationdict["B2"] = Button(text = "Password", command= lambda: self.__update_account_keyword("password"))
            self.locationdict["B2"].grid(row = 4, column = 3, pady = 2, padx = 2)
            self.locationdict["B3"] = Button(text = "Birthday", command= lambda: self.__update_account_keyword("birthday"))
            self.locationdict["B3"].grid(row = 5, column = 3, pady = 2, padx = 2)
            self.locationdict["B4"] = Button(text = "Phonenumber", command= lambda: self.__update_account_keyword("phone"))
            self.locationdict["B4"].grid(row = 6, column = 3, pady = 2, padx = 2)
        

    def __update_account_keyword(self, keyword: str) -> None:
        """
        Creates a screen for a user or admin to update a piece of info for a profile.
        
        Takes in the variable "keyword" to describe what the user is updating, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__update_account)
        if self.admin:
            self.locationdict[f"L0"] = Label(text = "Name of user:")
            self.locationdict[f"L0"].grid(row = 2, column = 1, pady = 2, padx = 2)
            self.locationdict[f"E0"] = Entry(self.master)
            self.locationdict[f"E0"].grid(row=2, column = 4, pady = 2, padx = 2)

            if keyword == "username":
                self.locationdict[f"L1"] = Label(text = "Please type in the new username: ")
                self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
            elif keyword == "password":
                self.locationdict[f"L1"] = Label(text = "Please type in the new password for the user (Must have 2 digits): ")
                self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
            elif keyword == "birthday":
                self.locationdict[f"L1"] = Label(text = "Please type in the new birthday (DD/MM/YYYY): ")
                self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
            elif keyword == "phone":
                self.locationdict[f"L1"] = Label(text = "Please type in the new phonenumber (7 letters long): ")
                self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
            elif keyword == "isadmin":
                self.locationdict[f"L1"] = Label(text = "Please type the users admin privilege (1 for yes, 0 for no): ")
                self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
            
            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row=3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="Update", command= lambda: self.__pass_update(keyword))
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2)            

        else:
            if keyword == "username":
                self.locationdict[f"L1"] = Label(text = "Please type in your new username: ")
                self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
            elif keyword == "password":
                self.locationdict[f"L1"] = Label(text = "Please type your new password (Must have 2 digits): ")
                self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
            elif keyword == "birthday":
                self.locationdict[f"L1"] = Label(text = "Please type in your new birthday (DD/MM/YYYY): ")
                self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
            elif keyword == "phone":
                self.locationdict[f"L1"] = Label(text = "Please type in your new phonenumber (7 letters long): ")
                self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
            
            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row=2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"L0"] = Label(text = "Please type in your password:")
            self.locationdict[f"L0"].grid(row = 3, column = 1, pady = 2, padx = 2)
            self.locationdict[f"E0"] = Entry(self.master)
            self.locationdict[f"E0"].grid(row=3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="Update", command=lambda: self.__pass_update(keyword))
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2)


    def __pass_update(self, keyword: str) -> None:
        """
        This function "passes" what the user wants to update for their profile, calling other files to update their specific information.
        
        Takes in the variable "keyword", to define what is being updated. Should not be called except under the appropriate circumstances.
        """
        if self.admin:
            message = self.user.update_account(self.locationdict["E0"].get(), "", keyword, self.locationdict["E1"].get(), self.admin)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                mb.showinfo(title = "Update User", message = message["message"])
                self.__back()
                self.__back()
        else:
            message = self.user.update_account(self.name, self.locationdict["E0"].get(), keyword, self.locationdict["E1"].get(), self.admin)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                if keyword == "username":
                    self.name = self.locationdict["E1"].get()
                mb.showinfo(title = "Update User", message = message["message"])
                self.__back()
                self.__back()


    def __delete_user(self) -> None:
        """
        Creates a screen for a user to delete their profile, or for an admin to delete any profile.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L0"] = Label(text = "Name of user to delete:")
            self.locationdict[f"L0"].grid(row = 2, column = 1, pady = 2, padx = 2)
            self.locationdict[f"E0"] = Entry(self.master)
            self.locationdict[f"E0"].grid(row=2, column = 4, pady = 2, padx = 2)
            self.locationdict[f"B1"] = Button(text="Confirm Deletion", command = self.__confirm_deletion_user)
            self.locationdict[f"B1"].grid(row=3, column = 3, pady = 2, padx = 2)    

        else:
            self.locationdict[f"L0"] = Label(text = "Password for confirmation:")
            self.locationdict[f"L0"].grid(row = 2, column = 1, pady = 2, padx = 2)
            self.locationdict[f"E0"] = Entry(self.master)
            self.locationdict[f"E0"].grid(row=2, column = 4, pady = 2, padx = 2)
            self.locationdict[f"B1"] = Button(text="Confirm Deletion", command = self.__confirm_deletion_user)
            self.locationdict[f"B1"].grid(row=3, column = 3, pady = 2, padx = 2)   

    
    def __confirm_deletion_user(self) -> None:
        """
        This function is activated when a user or admin deletes a profile, confirming their answer and informs them of the success or failure.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        answer = mb.askyesno(title = "Deletion Confirmation", message = "Are you sure you wish to delete the account?")
        if answer:
            if self.admin:
                message = self.user.delete_user(self.locationdict["E0"].get(), "", self.admin)
                if message["code"] == self.SUCCESS:
                    self.__back()
                else:
                    self.__printerror(message["message"])
            else:
                message = self.user.delete_user(self.name, self.locationdict["E0"].get(), self.admin)
                if message["code"] == self.SUCCESS:
                    self.__end()
                else:
                    self.__printerror(message["message"])
        else:
            self.__back()


    def __create_bus_stop(self) -> None:
        """
        Creates a screen for an admin to create a bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L1"] = Label(self.master, text = "Name of bus stop:")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L2"] = Label(self.master, text = "Latitude:")
        self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"L3"] = Label(self.master, text = "Longitude:")
        self.locationdict[f"L3"].grid(row = 4, column = 1, pady = 2, padx = 10, sticky=W)

        for i in range(3):
            self.locationdict[f"E{i+1}"] = Entry(self.master)
            self.locationdict[f"E{i+1}"].grid(row = i+2, column = 4, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__pass_bus_stop, text="Create Bus Stop")
        self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __pass_bus_stop(self) -> None:
        """
        This function "passes" the information from the "create bus stop" screen to other files to create a bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        name = self.locationdict["E1"].get()
        latitude = self.locationdict["E2"].get()
        longitude = self.locationdict["E3"].get()
        message = self.busstop.create_bus_stop(name, latitude, longitude)
        if message["code"] == self.SUCCESS:
            mb.showinfo(title = "Create Bus Stop", message = message["message"])
            self.__back()
        else:
            self.__printerror(message["message"])


    def __read_all_bus_stops(self) -> None:
        """
        Creates a screen for a user or admin to see all the bus stops in the database.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)
        
        message = self.busstop.read_bus_stops()
        listOfBusStops = message["parameters"]
        self.locationdict[f"L1"] = Label(text = "Name")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 2)
        self.locationdict[f"L2"] = Label(text = "Latitude")
        self.locationdict[f"L2"].grid(row = 2, column = 3, pady = 2, padx = 2)
        self.locationdict[f"L3"] = Label(text = "Longitude")
        self.locationdict[f"L3"].grid(row = 2, column = 4, pady = 2, padx = 2)
        for index, elem in enumerate(listOfBusStops):
            self.locationdict[f"L{(index+1)*4}"] = Label(text = elem["name"])
            self.locationdict[f"L{(index+1)*4}"].grid(row = index+3, column = 1, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*4+1}"] = Label(text = elem["latitude"])
            self.locationdict[f"L{(index+1)*4+1}"].grid(row = index+3, column = 3, pady = 2, padx = 2)
            self.locationdict[f"L{(index+1)*4+2}"] = Label(text = elem["longitude"])
            self.locationdict[f"L{(index+1)*4+2}"].grid(row = index+3, column = 4, pady = 2, padx = 2)


    def __update_bus_stop(self) -> None:
        """
        Creates a screen for an admin to select what to update for a bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L1"] = Label(text = "Please select what you'd like to change:")
        self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 2)
        self.locationdict["B1"] = Button(text = "Bus stop name", command= lambda: self.__update_bus_stop_keyword("name"))
        self.locationdict["B1"].grid(row = 3, column = 3, pady = 2, padx = 2)
        self.locationdict["B2"] = Button(text = "Latitude", command= lambda: self.__update_bus_stop_keyword("latitude"))
        self.locationdict["B2"].grid(row = 4, column = 3, pady = 2, padx = 2)
        self.locationdict["B3"] = Button(text = "Longitude", command= lambda: self.__update_bus_stop_keyword("longitude"))
        self.locationdict["B3"].grid(row = 5, column = 3, pady = 2, padx = 2)


    def __update_bus_stop_keyword(self, keyword: str) -> None:
        """
        Creates a screen for an admin to update a variable of a bus stop.
        
        Takes in the variable "keyword" to know what is being updated, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__update_bus_stop)

        self.locationdict[f"L0"] = Label(text = "Name of bus stop:")
        self.locationdict[f"L0"].grid(row = 2, column = 1, pady = 2, padx = 2)
        self.locationdict[f"E0"] = Entry(self.master)
        self.locationdict[f"E0"].grid(row=2, column = 4, pady = 2, padx = 2)

        if keyword == "name":
            self.locationdict[f"L1"] = Label(text = "Please type the new name for the bus stop: ")
            self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
        elif keyword == "latitude":
            self.locationdict[f"L1"] = Label(text = "Please type the new latitude for the bus stop: ")
            self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
        elif keyword == "longitude":
            self.locationdict[f"L1"] = Label(text = "Please type the new longitude for the bus stop: ")
            self.locationdict[f"L1"].grid(row = 3, column = 1, pady = 2, padx = 2)
        
        self.locationdict[f"E1"] = Entry(self.master)
        self.locationdict[f"E1"].grid(row=3, column = 4, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(text="Update", command= lambda: self.__pass_bus_update(keyword))
        self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2)   


    def __pass_bus_update(self, keyword: str) -> None:
        """
        This function "passes" the information from the "update bus stop" screen to other files to update a bus stop.
        
        Takes in the variable "keyword" to know what is being updated, and should not be called except under the appropriate circumstances.
        """
        message = self.busstop.update_bus_stop(self.locationdict["E0"].get(), keyword, self.locationdict["E1"].get())
        if message["code"] != self.SUCCESS:
            self.__printerror(message["message"])
        else:
            mb.showinfo(title = "Update Bus Stop", message = message["message"])
            self.__back()
            self.__back()


    def __delete_bus_stop(self) -> None:
        """
        Creates a screen for an admin to delete a bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L0"] = Label(text = "Name of bus stop to delete:")
        self.locationdict[f"L0"].grid(row = 2, column = 1, pady = 2, padx = 2)
        self.locationdict[f"E0"] = Entry(self.master)
        self.locationdict[f"E0"].grid(row=2, column = 4, pady = 2, padx = 2)
        self.locationdict[f"B1"] = Button(text="Confirm Deletion", command = self.__confirm_deletion_bus_stop)
        self.locationdict[f"B1"].grid(row=3, column = 3, pady = 2, padx = 2)  


    def __confirm_deletion_bus_stop(self) -> None:
        """
        Creates a prompt to guarantee that an admin wishes to delete a specific bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        answer = mb.askyesno(title = "Deletion Confirmation", message = "Are you sure you wish to delete this bus stop?")
        if answer:
            message = self.busstop.delete_bus_stop(self.locationdict["E0"].get())
            if message["code"] == self.SUCCESS:
                self.__back()
            else:
                self.__printerror(message["message"])
        else:
            self.__back()


    def __add_favorite_bus_stop(self) -> None:
        """
        Creates a screen for a user to favorite a bus stop. Also allows an admin to favorite a bus stop for any user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)
            self.locationdict[f"L2"] = Label(self.master, text = "Name of bus stop:")
            self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)
            self.locationdict[f"E2"] = Entry(self.master)
            self.locationdict[f"E2"].grid(row = 3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(self.master, command=self.__pass_favorite_bus_stop, text="Add Bus Stop")
            self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)

        else:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of bus stop:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(self.master, command=self.__pass_favorite_bus_stop, text="Add Bus Stop")
            self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)
        

    def __pass_favorite_bus_stop(self) -> None:
        """
        This function "passes" the information from the "favorite bus stop" screen to other files to favorite a bus stop for a user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        if self.admin:
            username = self.locationdict["E1"].get()
            busstop = self.locationdict["E2"].get()
            message = self.favbusstop.favorite_bus_stop(username, busstop)
            if message["code"] == self.SUCCESS:
                mb.showinfo(title = "Add Favorite Bus Stop", message = message["message"])
                self.__back()
            else:
                self.__printerror(message["message"])

        else:
            busstop = self.locationdict["E1"].get()
            message = self.favbusstop.favorite_bus_stop(self.name, busstop)
            if message["code"] == self.SUCCESS:
                mb.showinfo(title = "Add Favorite Bus Stop", message = message["message"])
                self.__back()
            else:
                self.__printerror(message["message"])


    def __delete_favorite_bus_stop(self) -> None:
        """
        Creates a screen to allow a user to unfavorite a bus stop. Also allows an admin to unfavorite a favorite bus stop for any user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)
            self.locationdict[f"L2"] = Label(self.master, text = "Name of bus stop:")
            self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)
            self.locationdict[f"E2"] = Entry(self.master)
            self.locationdict[f"E2"].grid(row = 3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(self.master, command=self.__pass_delete_favorite_bus_stop, text="Delete Bus Stop")
            self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)

        else:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of bus stop:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(self.master, command=self.__pass_delete_favorite_bus_stop, text="Delete Bus Stop")
            self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __pass_delete_favorite_bus_stop(self) -> None:
        """
        This function "passes" the information from the "delete favorite bus stop" screen to other files to remove a favorite bus stop for a user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        if self.admin:
            username = self.locationdict["E1"].get()
            busstop = self.locationdict["E2"].get()
            message = self.favbusstop.unfavorite_bus_stop(username, busstop)
            if message["code"] == self.SUCCESS:
                mb.showinfo(title = "Delete Favorite Bus Stop", message = message["message"])
                self.__back()
            else:
                self.__printerror(message["message"])

        else:
            busstop = self.locationdict["E1"].get()
            message = self.favbusstop.unfavorite_bus_stop(self.name, busstop)
            if message["code"] == self.SUCCESS:
                mb.showinfo(title = "Delete Favorite Bus Stop", message = message["message"])
                self.__back()
            else:
                self.__printerror(message["message"])


    def __read_favorite_bus_stops(self) -> None:
        """
        Creates a screen for a user to see all their favorite bus stops. Also allows an admin to read all the favorite bus stops of any user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(self.master, command=self.__pass_user_favorite_bus_stop, text="Find User Favorite")
            self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)

        else:
            message = self.favbusstop.get_favorite_bus_stops(self.name)
            listOfBusStops = message["parameters"]
            self.locationdict[f"L1"] = Label(text = "Favorite Bus Stops")
            self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 2)

            for index, elem in enumerate(listOfBusStops):
                self.locationdict[f"L{(index+1)*2}"] = Label(text = elem["bus_stop"])
                self.locationdict[f"L{(index+1)*2}"].grid(row = index+3, column = 3, pady = 2, padx = 2)


    def __pass_user_favorite_bus_stop(self) -> None:
        """
        This function "passes" the information from the "read favorite bus stop" screen to other files to show the favorite bus stops of a user. Used by admins.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        name = self.locationdict["E1"].get()
        message = self.user.get_user(name)
        if message["code"] == self.SUCCESS:
            self.__wipe_board()
            message = self.favbusstop.get_favorite_bus_stops(name)
            listOfBusStops = message["parameters"]
            self.locationdict[f"L1"] = Label(text = f"""Favorite Bus Stops of {name}""")
            self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 2)

            for index, elem in enumerate(listOfBusStops):
                self.locationdict[f"L{(index+1)*2}"] = Label(text = elem["bus_stop"])
                self.locationdict[f"L{(index+1)*2}"].grid(row = index+3, column = 3, pady = 2, padx = 2)
        else:
            self.__printerror(message["message"])
    

    def __check_weather_on_bus_stop(self) -> None:
        """
        Creates a screen for a user/admin to check the weather conditions of any bus stop in the system.

        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)        

        self.locationdict[f"L1"] = Label(self.master, text = "Name of bus stop:")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"E1"] = Entry(self.master)
        self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__pass_weather, text="Check Weather")
        self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __pass_weather(self) -> None:
        """
        This function "passes" the information from the "check weather on a bus stop" screen to other files to show the weather of a bus stop.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        message = self.weather.check_weather(self.locationdict["E1"].get())
        if message["code"] == self.SUCCESS:
            self.__wipe_board()

            self.locationdict[f"L1"] = Label(self.master, text = f"Temperature:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L2"] = Label(self.master, text = f"Humidity:")
            self.locationdict[f"L2"].grid(row = 2, column = 2, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L3"] = Label(self.master, text = f"Cloud Coverage:")
            self.locationdict[f"L3"].grid(row = 2, column = 3, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L4"] = Label(self.master, text = f"Wind:")
            self.locationdict[f"L4"].grid(row = 2, column = 4, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L5"] = Label(self.master, text = f"Wind Degrees:")
            self.locationdict[f"L5"].grid(row = 2, column = 5, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L6"] = Label(self.master, text = f"""{str(message["parameters"]["temp"])[0:4]}°C""")
            self.locationdict[f"L6"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L7"] = Label(self.master, text = f"""{str(message["parameters"]["humidity"])}%""")
            self.locationdict[f"L7"].grid(row = 3, column = 2, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L8"] = Label(self.master, text = f"""{str(message["parameters"]["clouds"])}%""")
            self.locationdict[f"L8"].grid(row = 3, column = 3, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L9"] = Label(self.master, text = f"""{str(message["parameters"]["wind"])[0:4]}Km/h""")
            self.locationdict[f"L9"].grid(row = 3, column = 4, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L10"] = Label(self.master, text = f"""{str(message["parameters"]["windDeg"])}°""")
            self.locationdict[f"L10"].grid(row = 3, column = 5, pady = 2, padx = 10, sticky=W)
            
        else:
            self.__printerror(message["message"])


    def __check_route_duration(self) -> None:
        """
        Creates a screen for a user/admin to check the duration of a route between any two bus stops in the system.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        self.locationdict[f"L1"] = Label(self.master, text = "Name of origin bus stop:")
        self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"E1"] = Entry(self.master)
        self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

        self.locationdict[f"L2"] = Label(self.master, text = "Name of destination bus stop:")
        self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

        self.locationdict[f"E2"] = Entry(self.master)
        self.locationdict[f"E2"].grid(row = 3, column = 4, pady = 2, padx = 2)

        self.locationdict[f"B1"] = Button(self.master, command=self.__pass_duration, text="Check Duration")
        self.locationdict[f"B1"].grid(row = 6, column = 3, pady=10, padx=10)


    def __pass_duration(self) -> None:
        """
        This function "passes" the information from the "check route duration" screen to other files to show the duration of a route.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        origin = self.locationdict[f"E1"].get()
        destination = self.locationdict[f"E2"].get()
        message = self.route.check_duration(origin, destination)
        if message["code"] == self.SUCCESS:
            self.__wipe_board()
            self.locationdict[f"L1"] = Label(self.master, text = f"""{message["parameters"]} Minutes""")
            self.locationdict[f"L1"].grid(row = 2, column = 3, pady = 2, padx = 10, sticky=W)
        else:
            self.__printerror(message["message"])


    def __buy_ticket(self) -> None:
        """
        Creates a screen for a user to buy a ticket and for an admin to buy a ticket for another user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user to buy ticket for:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="Buy Ticket", command=self.__pass_buy_ticket)
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2) 
        else:
            self.locationdict[f"B1"] = Button(text="Buy Ticket", command=self.__pass_buy_ticket)
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2) 


    def __pass_buy_ticket(self) -> None:
        """
        This function "passes" the information from the "buy ticket" screen to other files to buy a ticket for a user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        if self.admin:
            username = self.locationdict["E1"].get()
            message = self.ticket.buy_ticket(username)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                self.__back()
        else:
            message = self.ticket.buy_ticket(self.name)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                self.__back()


    def __use_ticket(self) -> None:
        """
        Creates a screen for a user to activate a ticket and for an admin to activate a ticket for another user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="See Unused Tickets", command= lambda: self.__admin_print_tickets(0))
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2) 

        else:
            ticket_dict = self.__get_ticket_dict(self.name)
            self.__print_tickets(self.name, ticket_dict, 0)
        
    
    def __pass_use_ticket(self, name, ticket_id) -> None:
        """
        This function "passes" the information from the "use ticket" screen to other files to activate a ticket for a user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        message = self.ticket.use_ticket(name, ticket_id)
        if message["code"] != self.SUCCESS:
            self.__printerror(message["message"])
        else:
            self.__back()


    def __ticket_history(self) -> None:
        """
        Creates a screen for a user to see their ticket history and for an admin to see the ticket history of any user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)
        
        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Name of user:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="See Ticket History", command= lambda: self.__admin_print_tickets(1))
            self.locationdict[f"B1"].grid(row=4, column = 3, pady = 2, padx = 2) 
        else:
            self.locationdict[f"L1"] = Label(self.master, text = "Ticket Status")
            self.locationdict[f"L1"].grid(row = 1, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L2"] = Label(self.master, text = "Ticket ID")
            self.locationdict[f"L2"].grid(row = 1, column = 3, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"L3"] = Label(self.master, text = "Time Stamp")
            self.locationdict[f"L3"].grid(row = 1, column = 4, pady = 2, padx = 10, sticky=W)

            ticket_dict = self.__get_ticket_dict(self.name)
            self.__print_tickets(self.name, ticket_dict, 1)


    def __gift_ticket(self) -> None:
        """
        Creates a screen for a user to be able to gift a ticket to another user
        and for an admin to gift a ticket from any user to any other user.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        self.__wipe_board()
        self.stack.append(self.__menu)

        if self.admin:
            self.locationdict[f"L1"] = Label(self.master, text = "Please type Ticket ID:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"L2"] = Label(self.master, text = "Please type in the user to gift from:")
            self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E2"] = Entry(self.master)
            self.locationdict[f"E2"].grid(row = 3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"L3"] = Label(self.master, text = "Please type in the user to gift to:")
            self.locationdict[f"L3"].grid(row = 4, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E3"] = Entry(self.master)
            self.locationdict[f"E3"].grid(row = 4, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="Gift Ticket", command=self.__pass_gift_ticket)
            self.locationdict[f"B1"].grid(row = 5, column = 3, pady = 2, padx = 2) 

        else:
            self.locationdict[f"L1"] = Label(self.master, text = "Please type Ticket ID:")
            self.locationdict[f"L1"].grid(row = 2, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E1"] = Entry(self.master)
            self.locationdict[f"E1"].grid(row = 2, column = 4, pady = 2, padx = 2)

            self.locationdict[f"L2"] = Label(self.master, text = "Please type in the user to gift to:")
            self.locationdict[f"L2"].grid(row = 3, column = 1, pady = 2, padx = 10, sticky=W)

            self.locationdict[f"E2"] = Entry(self.master)
            self.locationdict[f"E2"].grid(row = 3, column = 4, pady = 2, padx = 2)

            self.locationdict[f"B1"] = Button(text="Gift Ticket", command=self.__pass_gift_ticket)
            self.locationdict[f"B1"].grid(row = 4, column = 3, pady = 2, padx = 2) 


    def __pass_gift_ticket(self) -> None:
        """
        This function "passes" the information from the "gift ticket" screen to other files to gift a ticket between users.
        
        Takes in no variables, and should not be called except under the appropriate circumstances.
        """
        if self.admin:
            ticket_id = self.locationdict["E1"].get()
            owner = self.locationdict["E2"].get()
            gifter = self.locationdict["E3"].get()
            message = self.ticket.gift_ticket(ticket_id, owner, gifter)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                self.__back()
        else:
            ticket_id = self.locationdict["E1"].get()
            gifter = self.locationdict["E2"].get()
            message = self.ticket.gift_ticket(ticket_id, self.name, gifter)
            if message["code"] != self.SUCCESS:
                self.__printerror(message["message"])
            else:
                self.__back()


    def __print_tickets(self, name: str, ticket_dict: dict, sort: str) -> None:
        """
        This function displays tickets in two different ways for two different screens, "use ticket" and
        "ticket history"
        
        All it's variables are strings except ticket_dict, which is a dictionary.
        Cycles through the given ticket dict in two different ways depending on which "sort" is given (1, or 0).
        
        Should not be called except under the appropriate circumstances.
        """
        if sort == 0: # This prints the screen for unused tickets
            for index, elem in enumerate(ticket_dict["parameters"]):
                if elem["status"] == "unused":
                    ticket_id = elem["ticket_id"]
                    self.locationdict[f"B{index}"] = Button(self.master, command=lambda ticket_id=ticket_id: self.__pass_use_ticket(name, ticket_id), text="Activate Ticket")
                    self.locationdict[f"B{index}"].grid(row = index+2, column = 1, pady=2, padx=10)

                    self.locationdict[f"L{index}"] = Label(self.master, text = elem["ticket_id"])
                    self.locationdict[f"L{index}"].grid(row = index+2, column = 4, pady = 2, padx = 10, sticky=W)

        elif sort == 1: # This prints the screen for ticket history
            for index, elem in enumerate(ticket_dict["parameters"]):
                self.locationdict[f"L{(index+1)*4}"] = Label(self.master, text = elem["status"].upper())
                self.locationdict[f"L{(index+1)*4}"].grid(row = index+2, column = 1, pady = 2, padx = 10)

                self.locationdict[f"L{(index+1)*4+1}"] = Label(self.master, text = elem["ticket_id"])
                self.locationdict[f"L{(index+1)*4+1}"].grid(row = index+2, column = 3, pady = 2, padx = 10)

                if elem["time_activated"] != None:
                    self.locationdict[f"L{(index+1)*4+2}"] = Label(self.master, text = elem["time_activated"])
                    self.locationdict[f"L{(index+1)*4+2}"].grid(row = index+2, column = 4, pady = 2, padx = 10)
                else:
                    self.locationdict[f"L{(index+1)*4+2}"] = Label(self.master, text = "---")
                    self.locationdict[f"L{(index+1)*4+2}"].grid(row = index+2, column = 4, pady = 2, padx = 10)


    def __get_ticket_dict(self, name: str) -> None:
        """
        Minor function that is used to get a full ticket dictionary of a given user.

        Takes in the variable "name" which is the user who's history is wanted.
        """
        return self.ticket.read_all_tickets(name)


    def __admin_print_tickets(self, sort: int) -> None:
        """
        Minor function that is very similar to a "pass" function.
        Used by some ticket functions on the admin side to communicate with the __print_tickets function.
        """
        username = self.locationdict["E1"].get()
        self.__wipe_board()
        ticket_dict = self.__get_ticket_dict(username)
        self.__print_tickets(username, ticket_dict, sort)


    def __back(self) -> None:
        """
        This function can be used any time to go back to the previous screen. Used by the "back" button.
        
        Takes in no variables.
        """
        if len(self.stack) == 0:
            pass
        else:
            self.__wipe_board()
            new_menu = self.stack.pop()
            new_menu()


    def __wipe_board(self) -> None:
        """
        This function wipes any Labels, Entries and Buttons in self.locationdict to clear the screen for the next screen call.
        
        Takes in no variables.
        """
        for elem in self.locationdict:
            if (elem[0] == "L" or elem[0] == "E" or elem[0] == "B") and elem[1].isnumeric():
                self.locationdict[elem].destroy()


    def __printerror(self, stringmessage: str) -> None:
        """
        This function creates a messagebox to tell a user they've made an error.

        Takes in the variable "stringmessage" to see what to display.
        """
        mb.showerror(title="Error", message=stringmessage)


    def __end(self) -> None:
        """
        This function ends the Tkinter box and ends the UI run.
        """
        self.master.destroy()

main = UIAPI()