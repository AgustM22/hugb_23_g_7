from Client import Client
import json

class FavoriteBusStop(object):
    """
    This class is the UI class for any Favorite Bus Stop functions in the system.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        favorite_bus_stop(),
        unfavorite_bus_stop(),
        get_favorite_bus_stops().
    """
    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    
    def favorite_bus_stop(self, user: str, name: str) -> dict:
        """
        Sends information to the gRPC client to favorite a bus stop for a user.

        Takes in the variables "user" and "name", both strings. "user" is the name of the user, and "name" is the bus stop which he is favoriting. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.favorite_bus_stop(user, name)
        message = json.loads(message)
        return message
    
    def unfavorite_bus_stop(self, user: str, name: str) -> dict:
        """
        Sends information to the gRPC client to unfavorite a bus stop for a user.

        Takes in the variables "user" and "name", both strings. "user" is the name of the user, and "name" is the bus stop which he is unfavoriting. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.unfavorite_bus_stop(user, name)
        message = json.loads(message)
        return message

    def get_favorite_bus_stops(self, user: str) -> dict:
        """
        Fetches information from the gRPC client regarding the list of favorite bus stops a user has.

        Takes in the variable "user" (string). 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.get_favorite_bus_stops(user)
        message = json.loads(message)
        return message