from Client import Client
import json

class Weather(object):
    """
    This class is the UI class for any Weather functions in the system.
    Can be though of as a wrapper between the gRPC client and the UI layer.

    Methods:
        check_weather().
    """
    def __init__(self) -> None:
        """ 
        Initializing base variables. Provides no actions on it's own.
        """
        self.client = Client()
    
    def check_weather(self, name: str) -> dict:
        """
        Fetches information from the gRPC client regarding the weather conditions of a bus stop.

        Takes in the variable "name" for a bus stops name. 
        Returns a status and parameter dictionary from the server.
        """
        message = self.client.check_weather(name)
        message = json.loads(message)
        return message