'''----------------------------------------------------------------------------------------
FavoriteBusStopData : object

depends on
- UserData
- BusStopData
- DumbDB

METHODS
>
    __init__(self, database:object, user_data:UserData, bus_stop_data:BusStopData) -> None
    get_favorites_from_user(self, username) -> dict
    make_favorite(self, username:str, bus_stop_name:str, name:str=None) -> dict
    unfavorite(self, username:str, bus_stop_name:str) -> dict

----------------------------------------------------------------------------------------'''

from .userData import UserData
from .busStopData import BusStopData
from .DumbDB import DumbDB

class FavoriteBusStopData(object):
    """ Manages user data in the system. """

    def __init__(self, database:object, user_data:UserData, bus_stop_data:BusStopData) -> None:
        """ Initializes a FavoriteBusStop Table.
         
        Depends on a UserData table and BusStopData, since it joins the two """

        # Initialize table
        self.database:DumbDB = database
        database.create_table("favoriteBusStop", {"tuple_key":tuple(), "username":str(), "busStopName":str(), "name":str()}, "tuple_key")
        
        # initialize helpers
        self.bus_stop_data = bus_stop_data
        self.user_data = user_data

        # populate
        self.populate()


    def get_favorites_from_user(self, username) -> dict: 
        """ Gets a list of favorite bus_stops, given a users username (key) 
        
        username : str <- a key to a user in the system
        """

        return [{"bus_stop": i['busStopName'], "name": i['name']} for i in self.database.tables["favoriteBusStop"].get_rows(username, "username")]


    def make_favorite(self, username:str, bus_stop_name:str, name:str=None) -> dict: 
        """ Connects a bus stop and a user in the system, given their respective keys.
        Optionally, a name can be added to the connection.
         
        username : str <- a key to a user in the system
        bus_stop_name : str <- a key to a bus stop in the system
        name : str <- optional name for connection
        """
        # Check parameter types correct
        if (type(username) != str): raise Exception # wrong type for username :: PLACEHOLDER TODO
        if (type(bus_stop_name) != str): raise Exception # wrong type for bus_stop_name :: PLACEHOLDER TODO
        if (name is not None and type(name) != str): raise Exception # wrong type for name :: PLACEHOLDER TODO
        
        # objects exist
        if (not self.user_data.user_exists(username)): raise Exception # bus stop does not exist :: PLACEHOLDER TODO
        if (not self.bus_stop_data.bus_stop_exists(bus_stop_name)): raise Exception # bus stop does not exist :: PLACEHOLDER TODO

        # create row
        column = {"tuple_key": tuple([username, bus_stop_name]), "username": username, "busStopName": bus_stop_name}
        if (name is not None): column["name"] = name
        
        self.database.tables["favoriteBusStop"].create_row(column)

    def favorite_exists(self, username:str, bus_stop_name:str) -> bool:
        """ Returns True if a bus stop with the given name already exists within
        the system, else False. 

        username: str
        bus_stop_name: str """

        return self.database.tables["favoriteBusStop"].key_exists(tuple([username, bus_stop_name]))


    def unfavorite(self, username:str, bus_stop_name:str) -> dict:
        """ Removes a connection between a bus stop and a user in the favorites table, given their respective keys.

        username : str <- a key to a user in the system
        bus_stop_name : str <- a key to a bus stop in the system
        """

        if (self.user_data.user_exists(username)) and (self.bus_stop_data.bus_stop_exists(bus_stop_name)):
            self.database.tables["favoriteBusStop"].delete_row(tuple([username, bus_stop_name]))
            return True
        return False # if not exists, don't throw error


    def populate(self) -> None:
        """ A population script that injects sample data for users into the database. """
        self.make_favorite('Hemmi', 'Mjódd', 'Helvíti')
        self.make_favorite('Hemmi', 'Samgöngustofa')
        self.make_favorite('Dagur Maður', 'Eiðistorg', 'Rottu platz')
        self.make_favorite('Gulli', 'Mjódd', 'Sniðugt')
        # TODO add more!

if __name__ == '__main__':
    """ A utility for testing the FavoriteBusStopData class"""
    DB = DumbDB()
    U = UserData(DB)
    B = BusStopData(DB)
    F = FavoriteBusStopData(DB, U, B)

    print(F.get_favorites_from_user('Hemmi'))