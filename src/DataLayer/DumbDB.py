"""------------------------------------------------------------------------------------
DumbDB : Object
    May God help us if this ever makes it to production...
    NO PERSISTENCE!!! :,)

METHODS 
>
DumbDB : Object
    __init__(self)                                              -> DumbDB
    create_table(self, name:str, columns:dict, key_column:str)  -> Table
    delete_table(self, name:str)                                -> None
    list_tables(self)                                           -> list

DumbDB.TABLE : Object
>
    __init__(self, columns:dict, key_column:str)            -> DumbDB.Table 
    get_all_rows(self)                                      -> list
    get_rows(self, key:str, column:str)                     -> list
    get_row(self, key:str, column:str=None)                 -> dict
    get_column_names(self)                                  -> list
    key_exists(self, key:any)                               -> bool
    create_row(self, columns:dict)                          -> dict
    delete_row(self, id_key:str)                            -> dict
    update_row(self, id_key:str, columns:dict)              -> dict
------------------------------------------------------------------------------------"""

class DumbDB(object):
    """ An extremely dumb database (simulation) that is stored entirely locally :) """

    class Table(object):
        def __init__(self, columns:dict, key_column:str): 
            """ Initialize a table with rows, columns, and a key_column which can be
            used to easily access specific rows in the table. """

            self.key_column = key_column
            self.columns:dict = columns
            self.rows = dict()

            self.current_id = 0
        

        def get_all_rows(self):
            """ Reads and returns all rows as a list. """

            return [dict(a) for a in self.rows.values()]
        

        def get_rows(self, key:str, column:str) -> list:
            """ Allows an extremely basic form of where statement. 
            Searches the elements of a column and returns a list of rows where the element
            matches the key. 
            
            key: str (existing row in table)
            columns: str (existing column in row)
            """

            return [i for i in self.rows.values() if i[column] == key]
        
        
        def get_row(self, key:str, column:str=None) -> dict:
            """ Fetches the first row with an element matching the key, in
             the given row. (column defaults to key_column of table)
              
            key: str (existing row in table)
            column: str (existing column in row) """
            
            # No column specified (default behavior)
            if column is None: return self.rows[key]
            
            # Look for first row fitting the criteria
            for i in self.rows.values():
                if i[column] == key: return i

            return None # Does not exist
        
        
        def get_column_names(self) -> list:
            """ Returns the d names of all columns in the table. """

            return list(self.columns.keys())

        def key_exists(self, key) -> bool:
            """ Returns True if a key exists in the table, else False. 
            
            key: any (referencing row in table) (must be same type as table key parameter) """

            return key in self.rows.keys()


        def create_row(self, columns:dict) -> dict:
            """ Adds a row to the table. The row can be accessed directly via
            a key_column.
            
            Returns created row (as stored by the system) on success. 
            
            columns: dict -> {
              column1: object,
              columns2: object,
              ... etc. (matching table defined columns.)
            } """

            # EXCEPTION -> row input missing key_column
            if not self.key_column in columns: raise Exception() # Placeholder exception -> key is a required field
            
            # EXCEPTION -> column with same unique identifier already exists
            if self.key_exists(columns[self.key_column]): raise Exception() # Placeholder exception -> key is already in use.

            # All parameters have the correct respective types (see self.columns)
            for c in columns.keys():
                if not (type(self.columns[c]) == type(columns[c])): raise Exception() # Placeholder exception -> field has incorrect type.
            
            # Make sure row contains all (and only) the required columns for the table.
            for k in self.columns.keys():
                # Allow parameters (other than key_column) to be null.
                if k not in columns: columns[k] = None
            
            self.rows[columns[self.key_column]] = columns # Commit row to Table

            # Return confirmation of row creation.
            return self.get_row(columns[self.key_column])
        

        def delete_row(self, id_key:str) -> dict:
            """ Deletes a row from a table, given it's id_key 
            
            id_key: str (referencing row in table)
            """

            ret = self.rows[id_key] # fails if row does not exist
            del self.rows[id_key]

            return ret
        

        def update_row(self, id_key:str, columns:dict) -> dict:
            """ Updates a row with the given id_key, and changes it to match
            the given "columns" dictionary object. 
            On success, return the updated row. 

            id_key: str (referencing row in table)
            """

            if id_key == columns[self.key_column]:
                if not self.key_exists(columns[self.key_column]): raise Exception() # Placeholder exception key does not exist
                self.overwrite_row(columns)
            else:
                self.create_row(columns)
                self.delete_row(id_key)

            return self.get_row(columns[self.key_column])
        

        def overwrite_row(self, columns:dict) -> dict:
            """ Adds a row to the table. The row can be accessed directly via
            a key_column.
            
            Returns created row (as stored by the system) on success. 
            
            columns: dict -> {
              column1: object,
              columns2: object,
              ... etc. (matching table defined columns.)
            } """

            # EXCEPTION -> row input missing key_column
            if not self.key_column in columns: raise Exception() # Placeholder exception -> key is a required field
            
            # All parameters have the correct respective types (see self.columns)
            for c in columns.keys():
                if not (type(self.columns[c]) == type(columns[c])): raise Exception() # Placeholder exception -> incorrect field type
            
            # Make sure row contains all (and only) the required columns for the table.
            for k in self.columns.keys():
                # Allow parameters (other than key_column) to be null.
                if k not in columns: columns[k] = None
            
            self.rows[columns[self.key_column]] = columns # Commit row to Table

            # Return confirmation of row creation.
            return self.get_row(columns[self.key_column])
        

        def generate_id(self):
            """ gets a new unique id to use in the table. """

            # This is meant to be a stand-in for a safe id-generator
            self.current_id += 1
            return self.current_id


    def __init__(self):
        """ Initializes a database instance with a dictionary of tables, so that each
         table can be referenced via a name. """

        self.tables: dict = dict()


    def create_table(self, name:str, columns:dict, key_column:str):
        """ Creates a new table in the database. 
        
        name: str
        columns: dict -> {
          column1: object,
          column2: object,
          ... etc. (where 'column' is the name of the column and 'object' is an object of the same database that should be accepted)
        }
        key_column: str (the column name of the column that can be used to identify and retrieve specific rows.)"""
        
        # Create file to read table from.
        self.tables[name] = DumbDB.Table(columns, key_column)
    

    def delete_table(self, name:str):
        """ Deletes a table from the database. 
        
        name: str (name of existing table)"""
        
        if name not in self.tables:
            raise Exception() # temporary Placeholder (please :P )

        del self.tables[name]

        

    def list_tables(self):
        """ returns a list of all tables in the system. """

        return list(self.tables.keys())
