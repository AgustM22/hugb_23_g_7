"""----------------------------------------------------------------------------------------
UserData : object

USER METHODS
  get_user(name:str)
  get_all_users()
  create_user(columns:dict)
  delete_user(name:str)
  update_user(name:str, columns:dict)
  user_exists(name:str)
  populate()

User dictionary format * * * * * 
user = {
    "name":str(),
    "password":str(), 
    "birthday":date(2022,1,1), 
    "phone":str(), 
    "isadmin":bool()
}
----------------------------------------------------------------------------------------"""
from datetime import date

class UserData(object):
    """ Manages user data in the system. """

    def __init__(self, database:object) -> None:
        """ Initializes a UserData instance with a database to that is used to handle data.
        The database must be given at initialization.
        Runs a population script with sample data. 
        
        database: DumbDB instance"""

        # Initialize table
        self.database = database
        database.create_table("user", {"name":str(), "password":str(), "birthday":date(2022,1,1), "phone":str(), "isadmin":bool()}, "name")
        
        # populate
        self.populate()


    def get_user(self, name) -> dict: 
        """ Gets and returns a dictionary representation of a user, given his username. 
        
        name: str"""

        return self.database.tables["user"].get_row(name)

    
    def get_all_users(self) -> list:
        """ Returns a list of dict representations of existing users in the system. """
        
        return self.database.tables["user"].get_all_rows()


    def create_user(self, columns:dict) -> dict: 
        """ Creates a user in the "database" given the columns he should be associated with.
        
        Returns a dictionary representation of the user (as stored by the database) if successful. 
        
        columns: dict -> {
            name: str, 
            password: str, 
            birthday: datetime.date, 
            phone: str, 
            isadmin: bool
        } """

        self.database.tables["user"].create_row(columns)
        return self.get_user(columns["name"])


    def delete_user(self, name:str): 
        """ Deletes a user from the database, given their username. 
        
        Returns a dictionary representation of the deleted user if successful. 
        
        name: str """

        return self.database.tables["user"].delete_row(name)


    def update_user(self, name:str, columns:dict): 
        """ Updates a user with a given username in the database.
        Effectively an overwrite. 
        
        Returns a dictionary representation of the user (as stored by the database) if successful. 
        
        name: str
        columns: dict -> {
            name: str, 
            password: str, 
            birthday: datetime.date, 
            phone: str, 
            isadmin: bool
        } """

        return self.database.tables["user"].update_row(name, columns)


    def user_exists(self, name:str) -> bool:
        """ Returns True if a bus stop with the given name already exists within
        the system, else False. 

        name: str """

        return self.database.tables["user"].key_exists(name)


    def populate(self) -> None:
        """ A population script that injects sample data for users into the database. """

        self.create_user({"name": 'Jónatann', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False})
        self.create_user({"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False})
        self.create_user({"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False})
        self.create_user({"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False})
        self.create_user({"name": 'Gusty', "password": 'sunshine', "birthday": date(1990, 9, 14), "phone": '003545812345', "isadmin": False})
        self.create_user({"name": 'Gulli', "password": 'letmein', "birthday": date(1995, 8, 25), "phone": '003545642345', "isadmin": False})
        self.create_user({"name": 'Alfreð', "password": 'dragon', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": False})
        self.create_user({"name": '', "password": '', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": True})
        self.create_user({"name": '1', "password": '', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": False})