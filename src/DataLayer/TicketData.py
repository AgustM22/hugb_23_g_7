"""----------------------------------------------------------------------------------------
TicketData : object

TICKET METHODS
    get_ticket(self, ticket_id) -> dict: 
    get_all_tickets(self) -> list:
    get_tickets_by_user(self, username) -> list:
    create_ticket(self, columns:dict) -> dict:
    delete_ticket(self, name:str):
    update_ticket(self, ticket_id:str, columns:dict):
    ticket_exists(self, ticket_id:str) -> bool: 
    populate(self) -> None:


User dictionary format * * * * * 
ticket = {
    "ticket_id":str(),
    "owner":str(),                         // references a username of a user in the system
    "time_activated":datetime.datetime(),  // Or None if not activated yet
    "type":int()                           // for future utility (variable ticket duration)
}
----------------------------------------------------------------------------------------"""
from datetime import datetime

class TicketData(object):
    """ Manages user data in the system. """

    def __init__(self, database:object) -> None:
        """ Initializes a ticket data instance with a database.
        Creates a ticket table in the database with the appropriate columns.
        Finally it populates the database with some basic data. """

        # Initialize table
        self.database = database
        database.create_table("ticket", {"ticket_id":str(), "owner":str(), "time_activated":datetime(2022,1,1), "type":int()}, "ticket_id")
        
        # populate
        self.populate()


    def get_ticket(self, ticket_id) -> dict: 
        """ Gets and returns a dictionary representation of a user, given his username. 
        
        name: str"""

        return self.database.tables["ticket"].get_row(ticket_id)

    
    def get_all_tickets(self) -> list:
        """ Returns a list of dict representations of existing tickets in the system. """
        
        return self.database.tables["ticket"].get_all_rows()
    

    def get_tickets_by_user(self, username) -> list: # 
        """ Returns a list of tickets connected to a certain user, given his username

        username: str  --> identifying an existing user in the system 
        """
        return self.database.tables["ticket"].get_rows(username, "owner")


    def create_ticket(self, columns:dict) -> dict:
        """ Creates a unique and independently identifiable ticket in the system
        connected to a user.
         
        columns: dict -> {
            "ticket_id":str(),
            "owner":str(),                         // references a username of a user in the system
            "time_activated":datetime.datetime(),  // Or None if not activated yet
            "type":int()                           // for future utility (variable ticket duration)
        } All attributes are optional, an empty dict is also valid. """

        columns["ticket_id"] = str(self.database.tables["ticket"].generate_id())
        self.database.tables["ticket"].create_row(columns)
        return self.get_ticket(columns["ticket_id"])


    def delete_ticket(self, ticket_id:str): # TODO only callable by admin
        """ Deletes a database in the system.
        This functions should only be callable by an admin.
         
        ticket_id : str -> identifies the ticket to delete """

        return self.database.tables["ticket"].delete_row(ticket_id)


    def update_ticket(self, ticket_id:str, columns:dict):
        """ Updates a ticket with a given ticket_id in the database.
        Effectively an overwrite. 
        
        ticket_id : str -> identifies a ticket in the database
        columns : dict  -> with appropriate columns. Replaces the current columns of a given row
        """
        if (self.get_ticket(ticket_id)["time_activated"] is None) and columns["time_activated"] is None:
            columns.pop("time_activated")

        if columns["ticket_id"] != ticket_id: 
            raise Exception() # trying to change ticket_id!  TODO temporary
        
        return self.database.tables["ticket"].update_row(ticket_id, columns)


    def ticket_exists(self, ticket_id:str) -> bool: 
        """ Returns true if a ticket with a given ticket id exists in the system

        ticket_id: str -> identifies the ticket"""

        return self.database.tables["ticket"].key_exists(ticket_id)


    def populate(self) -> None:
        """ A population script that injects sample data for tickets into the database. """
        self.create_ticket({"owner": "Gulli", "type": 1})
        self.create_ticket({"owner": "Hemmi", "type": 1})
        self.create_ticket({"owner": "Jón G", "type": 1})
        self.create_ticket({"owner": "Jónatann", "type": 1})