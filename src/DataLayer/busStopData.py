"""----------------------------------------------------------------------------------------
BusStopData : object

BUS STOP METHODS
>
  create_bus_stop(self, columns:dict)           -> dict
  get_bus_stop(self, name:str)                  -> dict
  get_all_bus_stops(self)                       -> list
  update_bus_stop(self, name:str, columns:dict) -> dict
  delete_bus_stop(self, name:str)               -> dict
  bus_stop_exists(self, name:str)               -> bool
  populate(self) -> None:

  
BUS STOP DICTIONARY FORMAT
>
bus_stop = {
    "name":str()
}
----------------------------------------------------------------------------------------"""

class BusStopData(object):
    """ Manages bus stops in the system. """

    def __init__(self, database) -> None:
        """ Initializes a BusStopData instance with a database too that is used to handle data.
        The database must be given at initialization.
        Runs a population script with sample data. """
        
        # Initialize table
        self.database = database
        self.database.create_table("bus_stop", {"name":str(), "latitude":float(), "longitude":float()}, "name")

        self.populate()


    def create_bus_stop(self, columns:dict) -> dict:
        """ Creates a bus stop in the "database" given the columns it should be associated with.
         Returns a dictionary representation of the user (as stored by the database) if successful. 
         
        columns: dict -> {
            name: str
        } """
        
        return self.database.tables["bus_stop"].create_row(columns)


    def get_bus_stop(self, name:str) -> dict:
        """ Gets and returns a dictionary representation of a bus stop, given its name. 
        
        name: str """

        return self.database.tables["bus_stop"].get_row(name)


    def get_all_bus_stops(self) -> list:
        """ Returns a list of dict representations of existing users in the sytem. """

        return self.database.tables["bus_stop"].get_all_rows()


    def update_bus_stop(self, name:str, columns:dict) -> dict:
        """ Updates a bus stop with a given name in the database.
        Effectively an overwrite. 
        
        Returns a dictionary representation of the name (as stored by the database) if successful. 
        
        name: str
        columns: dict -> {
            name: str
        } """
        
        return self.database.tables["bus_stop"].update_row(name, columns)


    def delete_bus_stop(self, name:str) -> dict:
        """ Deletes a bus stop from the database, given its name.
        
        Returns a dictionary representation of the deleted bus stop if successful.
        
        name: str 
        columns: dict -> {
            name: str
        } """

        return self.database.tables["bus_stop"].delete_row(name)
    

    def bus_stop_exists(self, name:str) -> bool:
        """ Returns True if a bus stop with the given name already exists within
        the system, else False. 
        
        name: str """

        return self.database.tables["bus_stop"].key_exists(name)


    def populate(self) -> None:
        """ A population script that injects sample data for users into the database. """

        self.create_bus_stop({"name": 'Mjódd', "latitude": 64.109701999999999, "longitude": -21.842642999999999})
        self.create_bus_stop({"name": 'Samgöngustofa', "latitude": 64.136689000000004, "longitude": -21.887231})
        self.create_bus_stop({"name": 'Grænamýri', "latitude": 64.146191000000002, "longitude": -21.977658999999999})
        self.create_bus_stop({"name": 'Eiðistorg', "latitude": 64.150386999999995, "longitude": -21.9861})
        self.create_bus_stop({"name": 'Kaplaskjól', "latitude": 64.144683000000001, "longitude": -21.97099})
        self.create_bus_stop({"name": 'Hlemmur', "latitude": 64.143253000000001, "longitude": -21.914211000000002})