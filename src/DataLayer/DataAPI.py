"""----------------------------------------------------------------------------------------
DataAPI : object
    self.DB

METHODS
>
    // User  methods
    create_user(self, user:dict)                    -> dict
    get_user(self, username:str)                    -> dict
    update_user(self, username:str, user:dict)      -> dict
    delete_user(self, username:str)                 -> dict
    user_exists(self, username:str)                 -> bool
    get_all_users(self)                             -> list
    
    // Bus stop methods
    create_bus_stop(self, bus_stop:dict)            -> dict 
    get_bus_stop(self, name:str)                    -> dict 
    delete_bus_stop(self, name:str)                 -> dict 
    update_bus_stop(self, name:str, bus_stop:dict)  -> dict 
    bus_stop_exists(self, name:str)                 -> bool
    get_all_bus_stops(self):                        -> list

    // Favorite bus stop methods
    favorite_bus_stop(self, username:str, bus_stop_name:str, name:str=None) -> None
    unfavorite_bus_stop(self, username:str, bus_stop_name:str) -> None
    get_favorites_from_user(self, username:str)                -> list

    // Ticket methods
    get_ticket(self, ticket_id)                      -> dict
    get_all_tickets(self)                            -> list
    get_tickets_by_user(self, username)              -> list
    create_ticket(self, columns:dict)                -> dict
    delete_ticket(self, ticket_id:str)               -> dict
    update_ticket(self, ticket_id:str, columns:dict) -> dict
    ticket_exists(self, ticket_id:str)               -> bool

    
DATA STRUCTURES FOR MODELS
>
    user_dict{
        "name": name:str, 
        "password": password:str, 
        "birthday": birthday:datetime.date, 
        "phone": phone:str
        "isadmin": isadmin:bool
    }

    bus_stop_dict{
        "name": name:str, 
    }

DEPENDENCIES
>
    DumbDB : object
    userData : object
    busStopData : object

----------------------------------------------------------------------------------------"""
from .DumbDB import DumbDB
from .userData import UserData
from .busStopData import BusStopData
from .FavoriteBusStopData import FavoriteBusStopData
from .TicketData import TicketData

class DataAPI(object):
    """ An interface used (mainly by the logic layer) to interact with the data layer."""

    # Define some object instances that can be used by the entire data layer (reduces redundant object creation)
    DB = DumbDB()
    USER_DATA = UserData(DB)
    BUS_STOP_DATA = BusStopData(DB)
    FAVORITE_BUS_STOP_DATA = FavoriteBusStopData(DB, USER_DATA, BUS_STOP_DATA)
    TICKET = TicketData(DB)

    def __init__(self):
        """ Initializes a DataAPI instance. """
        pass

    def reset(self):
        """ Resets the database. """
        DataAPI.DB = DumbDB()
        DataAPI.USER_DATA = UserData(DataAPI.DB)
        DataAPI.BUS_STOP_DATA = BusStopData(DataAPI.DB)
        DataAPI.FAVORITE_BUS_STOP_DATA = FavoriteBusStopData(DataAPI.DB, DataAPI.USER_DATA, DataAPI.BUS_STOP_DATA)

    # * * *   USER METHODS   * * * * * -----------------------------------------

    def create_user(self, user:dict) -> dict: 
        """ Creates a user instance in the data layer.
        If successful, returns the user instance 
        
        user: dict -> {
            name: str, 
            password: str, 
            birthday: datetime.date, 
            phone: str, 
            isadmin: bool
        }"""

        return DataAPI.USER_DATA.create_user(user)


    def get_user(self, username:str) -> dict: 
        """ gets a user instance from the data layer, given his username (a unique identifier) 
        
        username: str (referencing an existing user)
        """

        return DataAPI.USER_DATA.get_user(username)


    
    def update_user(self, username:str, user:dict) -> dict: 
        """ Updates a user instance in the data layer.
        If successful, returns the user instance.
        
        username: str
        user: dict -> {
            name: str, 
            password: str, 
            birthday: datetime.date, 
            phone: str, 
            isadmin: bool
        }"""
        
        return DataAPI.USER_DATA.update_user(username, user)


    def delete_user(self, username:str) -> dict: 
        """ Deletes a user instance in the data layer, given their username (unique identifier).
        If successful, returns the user instance. 
        
        username: str (referencing an existing user in the system)"""
        
        return DataAPI.USER_DATA.delete_user(username)


    def user_exists(self, username:str) -> bool:
        """ Returns True if a user with the given username already exists within
        the system, else False.
        
        username: str """

        return DataAPI.USER_DATA.user_exists(username)


    def get_all_users(self) -> list: 
        """ Gets a list of all user instances that exist in the data layer """

        return DataAPI.USER_DATA.get_all_users()




    # * * *   BUS STOP METHODS   * * * * * -------------------------------------
    
    def create_bus_stop(self, bus_stop:dict) -> dict: 
        """ Creates a bus stop instance in the data layer. 
        Returns the instance on success.
        
        bus_stop: dict -> {
            name: str
        }"""

        return DataAPI.BUS_STOP_DATA.create_bus_stop(bus_stop)


    def get_bus_stop(self, name:str) -> dict: 
        """ Gets and returns a specific bus_stop given its name (unique identifier) 
        
        name: str
        """
        
        return DataAPI.BUS_STOP_DATA.get_bus_stop(name)


    def delete_bus_stop(self, name:str) -> dict: 
        """ Deletes a bus stop given its name (unique identifier). 
        Returns the instance on success.
        
        name: str (referencing an existing bus_stop in the system)
        """

        return DataAPI.BUS_STOP_DATA.delete_bus_stop(name)


    def update_bus_stop(self, name:str, bus_stop:dict) -> dict: 
        """ Updates a bus stop given its name (unique identifier) and a dict
        with the values to replace.
        Returns the instance on success. 
        
        name: str
        columns: dict -> {
            name: str
        } """
        
        return DataAPI.BUS_STOP_DATA.update_bus_stop(name, bus_stop)
    

    def bus_stop_exists(self, name:str) -> bool:
        """ Returns True if a bus stop with the given name already exists within
        the system, else False.
        
        name: str
        """

        return DataAPI.BUS_STOP_DATA.bus_stop_exists(name)


    def get_all_bus_stops(self) -> list: 
        """ Gets a list of all bus stop instances that exist in the data layer. """

        return DataAPI.BUS_STOP_DATA.get_all_bus_stops()
    

    # FAVORITE BUS STOP ---------------------------------------------------------
     
    def favorite_bus_stop(self, username:str, bus_stop_name:str, name:str=None) -> None:
        """ makes a given bus stop a favorite bus stop for a given user.
        Optionally, add a name to the bus stop.
        
        username : str <- a key to a user in the system
        bus_stop_name : str <- a key to a bus stop in the system
        name : str <- optional name for connection 
        """
        return DataAPI.FAVORITE_BUS_STOP_DATA.make_favorite(username, bus_stop_name, name)


    def unfavorite_bus_stop(self, username:str, bus_stop_name:str) -> None:
        """ Unfavorite a given bus stop for a given user.

        username : str <- a key to a user in the system
        bus_stop_name : str <- a key to a bus stop in the system
        """
        return DataAPI.FAVORITE_BUS_STOP_DATA.unfavorite(username, bus_stop_name)


    def get_favorites_from_user(self, username:str) -> list:
        """ Gets a list of bus stops a user has marked as favorite.

        Parameters:
            Username : str <- a key to a user in the system
        
        Returns a list of dictionaries of the format : dict {
            "BusStopName" : str,
            "name" : str
        }
        """
        return DataAPI.FAVORITE_BUS_STOP_DATA.get_favorites_from_user(username)

    def favorite_exists(self, username: str, bus_stop_name: str) -> bool:
        """ Check if a given busstop is favorited by given user.

        username : str <- a key to a user in the system
        bus_stop_name : str <- a key to a bus stop in the system
        """
        return DataAPI.FAVORITE_BUS_STOP_DATA.favorite_exists(username, bus_stop_name)
    

    # * * *   Ticket   * * * * * -----------------------------------------------
    def get_ticket(self, ticket_id) -> dict: 
        """ Gets a dictionary representation of a given ticket from the database.
         
        ticket_id : str -> identifies a ticket """
        return self.TICKET.get_ticket(ticket_id)


    def get_all_tickets(self) -> list:
        """ returns a list of all tickets in the system. """
        return self.TICKET.get_all_tickets()


    def get_tickets_by_user(self, username) -> list:
        """ Gets all tickets that a given user owns.
         
        Username : str -> identifies a specific user / owner """
        return self.TICKET.get_tickets_by_user(username)


    def create_ticket(self, columns:dict) -> dict:
        """ Creates a ticket.
         
        Columns : dict {
            "owner":str(),                         // references a username of a user in the system
            "time_activated":datetime.datetime(),  // Or None if not activated yet
            "type":int()                           // for future utility (variable ticket duration)
        } """
        return self.TICKET.create_ticket(columns)


    def delete_ticket(self, ticket_id:str):
        """ Deletes a ticket from the database given its identifier
        
        ticket_id : str -> identifies a ticket in the database.    """
        return self.TICKET.delete_ticket(ticket_id)


    def update_ticket(self, ticket_id:str, columns:dict):
        """ Updates a ticket (overwrites with new data).
         
        ticket_id : str -> identifies a ticket in the database
        columns : dict {
            "owner":str(),                         // references a username of a user in the system
            "time_activated":datetime.datetime(),  // Or None if not activated yet
            "type":int()                           // for future utility (variable ticket duration)        
        } """
        return self.TICKET.update_ticket(ticket_id, columns)


    def ticket_exists(self, ticket_id:str) -> bool: 
        """ Returns True if a ticket (given its identifier) exists, else False.
         
        ticket_id : str -> identifies a ticket in the database. """

        return self.TICKET.ticket_exists(ticket_id)




