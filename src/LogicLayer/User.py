import json
from DataLayer.DataAPI import DataAPI
from datetime import datetime

class User:
    """ A Class that performs operations for the LogicAPI folder relating to Users."""
    def __init__(self):
        self.code = 405 #Code = 405 is an error. Code = 200 is a success
        self.message = "None" #Error message
        self.status = {"code":self.code,"message": self.message} #Each check returns a status
        self.data = DataAPI()


    #---------- USER  CHECKS -----------
    #Checks for variables of a user

    def check_user_username(self, name:str) -> dict:
        """Checks if the given username is a valid username
        
        Returns:
            dict["code"] = 200 or 405

            dict["message"] = "Passed" or "Failed

        Parameters:
            name: str. Should be between 2-100 in length.
                

        """
        if len(name) < 2 or len(name) > 100: #Checks if username is between 2 or 100 characters long.
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid username, Username is either too long or too short"
            return self.status
        else:
            self.status["code"] = 200
            self.status["message"] = "Passed: Valid Username"
            return self.status
        
    def check_user_password(self,password:str):
        """Checks if given password is a valid password
        
        Returns:
            dict["code"] = 200 or 405
            
            dict["message"] = "Passed" or "Failed
        
        
        Parameters:
            password: str. Should be between 7-100 in length.
        """
        
        if len(password) < 7 or len(password) > 100: # checks if password is of length greater or less then 100
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid password, given password was too short or too long."
            return self.status
        
        numbercount = 0
        for char in password: # Counts number of digits
            if char.isdigit():
                numbercount += 1
        if numbercount < 2: #Raise error if password doesn't contain atleast 2 digits
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid passwordInvalid password, given password doesn't contain atleast 2 digits."
            return self.status
        
        else:
            self.status["code"] = 200
            self.status["message"] = "Passed: Valid Password"
            return self.status

    def check_user_birthday(self,birthday:str):
        """Checks if birthday perameter is valid

        Returns:
            dict["code"] = 200 or 405
            
            dict["message"] = "Passed" or "Failed
        
        Parameters:
            birthday: str. Should be formated DD/MM/YYYY

        """
        if len(birthday) != 10:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid birthday, given string was not exactly 10 digits long."
            return self.status
        
        birthdaylist = birthday.split("/")
        if len(birthdaylist[0]) != 2 or birthdaylist[0].isdigit() != True: #Invalid day 
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid birthday,  given string did not have correct format for day.."
            return self.status
        
        elif len(birthdaylist[1]) != 2 or birthdaylist[1].isdigit() != True: #Invalid month
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid birthday, given string did not have correct format for month."
            return self.status
        
        elif len(birthdaylist[2]) != 4 or birthdaylist[2].isdigit() != True: #Invalid year
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid birthday, given string did not have correct format for year."
            return self.status

        else: 
            self.status["code"] = 200
            self.status["message"] = "Passed: Valid Birthday"
            return self.status

    def check_user_phone(self,phone:str):
        """Checks if the given phone number is a valid

        Returns:
            dict["code"] = 200 or 405

            dict["message"] = "Passed" or "Failed
        
        Parameters:
            phone: str. Should be only numbers AND only be 7 in length

        """

        if phone.isdigit() != True or len(phone) != 7:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid phone number, given string was either wrong size or not a number."
            return self.status
        else:
            self.status["code"] = 200
            self.status["message"] = "Passed: Valid Phone"
            return self.status

    def check_user_isadmin(self,isadmin:str):
        """Checks if the isadmin paramter is valid

        Returns:            
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed
        

        Parameters:
            isadmin: str . Should be either 1 or 0

        """
        if isadmin != "0" and isadmin != "1":
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid isadmin, given string was neither 1 nor 0."
            return self.status
        else:
            self.status["code"] = 200
            self.status["message"] = "Passed: Valid isadmin"
            return self.status

    def confirm_password(self,username:str,password:str) -> bool:
        """Checks if the given password matches the given username
        
        Return:
            Bool
        
        Parameters:
            username: str
            password: str
        
        """
        if(self.data.user_exists(username)):
            user = self.get_user(username=username)
            if user[1]["password"] == password:
                return True
            else:
                return False
        else:
            return False


    #---------- CRUD USER  ---------

    # --- CREATE USER ---

    def create_user(self, name:str, password:str, birthday:str, phone:str, isadmin:str) ->dict:
        """
        Checks whether given parameters is valid for a user.
        returns error if not else creates the user in the database
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed
        
        Parameters:
            name: str. Should be between 2-100 in length
            password: str. Should be 2-100 in length
            birthday: str. Should be formated DD/MM/YYYY
            phone: str. Should be only Numbers AND 7 in length.
            isadmin: str. Should be 1 or 0
        """

        # Username checks
        username_check = self.check_user_username(name)
        if username_check["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        # Password checks
        password_check = self.check_user_password(password)
        if password_check["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        # Birthday checks.
        birthday_check = self.check_user_birthday(birthday)
        if birthday_check["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        # Phone number check
        phone_check = self.check_user_phone(phone)
        if phone_check["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        # isamind check
        isadmin_check = self.check_user_isadmin(isadmin)
        if isadmin_check["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        #The username should be destinct
        if self.data.user_exists(name): 
            self.status["code"] = 405
            self.status["message"] = "Error. Username is already taken."
            ret_dict = json.dumps(self.status)
            return ret_dict
        
        # Creating the new user via sending dict
        isadmin = (True if (isadmin == "1") else False)
        new_user_dict = {"name": name, "password": password, "birthday": datetime.strptime(birthday, "%d/%m/%Y").date(), "phone": phone, "isadmin": isadmin}
        self.data.create_user(new_user_dict) #Storing the user in the data base
        self.status["code"] = 200
        self.status["message"] = "Success"
        ret_dict = json.dumps(self.status)
        return ret_dict
    
    # --- READ USER ---

    def authorize(self, name:str, password:str):
        """ Checks if the given user_name and password match and returns string of the user_name and the isadmin variable. """
        #TODO: missing input parameters and output in docstring
        #TODO: Function should only have one return type.
        #TODO: Refactor to use the new self.status["parameters"] variable
        if self.confirm_password(name, password):
            isadmin = self.get_user(name)[1]["isadmin"]
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = [name, isadmin]
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed, Invalid password and or username"
            self.status["parameters"] = []
            ret_dict = json.dumps(self.status)
            return ret_dict


    def read_all_users(self) -> dict:
        """Gets all users from the database
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed
            dict["parameters"] = list of dicts. each dict is a user

        Parameters:
            None

        """
        allusers = self.data.get_all_users()
        self.status["code"] = 200
        self.status["message"] = "Success"
        for elem in allusers:
            elem["birthday"] = elem["birthday"].strftime("%m/%d/%Y")
        self.status["parameters"] = allusers
        ret_dict = json.dumps(self.status)
        return ret_dict
    
    def get_user(self,username:str):
        """Gets a sepcific user by username"""
        if (self.data.user_exists(username)):
            user = self.data.get_user(username)
            self.status["code"] = 200
            self.status["message"] = "Passed: User exists"
            return self.status, user
        else:
            self.status["code"] = 405
            self.status["message"] = "Passed: User doesn't exist"
            return self.status 

    def get_all_user(self, username: str):
        """Gets a sepcific user by username and returns all their information as a stringified json dict."""
        if self.data.user_exists(username):
            user = self.data.get_user(username)
            newuser = {}
            newuser["name"] = user["name"]
            newuser["password"] = user["password"]
            newuser["birthday"] = user["birthday"].strftime("%m/%d/%Y")
            newuser["phone"] = user["phone"]
            newuser["isadmin"] = user["isadmin"]
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = newuser
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Error. User does not exist."
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
            

    # --- UPDATE USER ---


    def update_user_username(self,old_username:str,password:str,new_username:str,isadmin:bool) -> dict:
        """
        Updates the given users username in the database.

        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            old_username:str. Should be a username that exists in the database
            password: str. Should match the user in the database
            new_username: str. Should be of length 2-100
            isadmin:bool. True or False. If true allows to give a empty password string
        
        """
        check_username = self.check_user_username(new_username)
        if(check_username["code"] == 200): #Check if the new user name is valid
            if (self.data.user_exists(old_username)):
                if (isadmin): #Bypass password check if an admin is updating the user.
                    user = self.get_user(username=old_username)
                    user[1]["name"] = new_username
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=old_username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                if (self.confirm_password(username=old_username,password=password)): # Check if the given password matches the one in the db
                    user = self.get_user(username=old_username)
                    user[1]["name"] = new_username
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=old_username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Username does not match password"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: User does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            ret_dict = json.dumps(check_username)
            return ret_dict #If the new user name is invalid
        
    def update_user_password(self,username:str,old_password:str,new_password:str,isadmin:bool) ->dict:
        """
        Updates the given users password
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            username:str. Should be a username that exists in the database
            password: str. Should match the user in the database
            new_password: str. Should be between 7-100 in length.
            isadmin:bool. True or False. If true allows to give a empty password string
        
        
        """
        check_password = self.check_user_password(new_password)
        if(check_password["code"] == 200): #Check if the new password is valid
            if (self.data.user_exists(username)):
                if(isadmin): #Bypass password check if an admin is updating the user.
                    user = self.get_user(username=username)
                    user[1]["password"] = new_password
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                if (self.confirm_password(username=username,password=old_password)): #Confirm the old password matches the one in the db
                    user = self.get_user(username=username)
                    user[1]["password"] = new_password
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Username does not match password"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: User does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            ret_dict = json.dumps(check_password)
            return ret_dict # If the new password isn't valid
        
    def update_user_phone(self,username:str,password:str,new_phone:str,isadmin) -> dict:
        """
        Updates the given users phone number
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            username:str. Should be a username that exists in the database
            password: str. Should match the user in the database
            new_phone: str. Should be only numbers AND of length 7.
            isadmin:bool. True or False. If true allows to give a empty password string
        """
        check_phone = self.check_user_phone(new_phone)
        if (check_phone["code"] == 200): #Checks if the new phone is valid
            if (self.data.user_exists(username)):
                if (isadmin): #Bypass password check if an admin is updating the user.
                    user = self.get_user(username=username)
                    user[1]["phone"] = new_phone
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                if (self.confirm_password(username=username,password=password)): #Checks if the given password matches the one in the db
                    user = self.get_user(username=username)
                    user[1]["phone"] = new_phone
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Username does not match password"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: User does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            ret_dict = json.dumps(check_phone)
            return ret_dict #If the given phone number is invalid
        
    def update_user_birthday(self,username:str,password:str,new_birthday:str,isadmin:bool) -> dict:
        """
        Updates the users password
                
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            username: str. Should be a username that exists in the database
            password: str. Should match the user in the database
            new_birthday: str. Format should be DD/MM/YYYY
            isadmin: bool. True or False. If true allows to give a empty password string
        
        """
        check_birhday = self.check_user_birthday(new_birthday)
        if(check_birhday["code"] == 200): #Checks if the new birthday is valid
            if (self.data.user_exists(username)):
                if (isadmin): #Bypass password check if an admin is updating the user.
                    user = self.get_user(username=username)
                    user[1]["birthday"] = datetime.strptime(new_birthday, "%d/%m/%Y").date()
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                if (self.confirm_password(username=username,password=password)): #Checks if the given password matches the one in the db
                    user = self.get_user(username=username)
                    user[1]["birthday"] = datetime.strptime(new_birthday, "%d/%m/%Y").date()
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Username does not match password"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: User does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            ret_dict = json.dumps(check_birhday)
            return ret_dict #if the given birthday is invalid return error 405.
        
    def update_user_isadmin(self,username:str,password:str,new_isadmin:str,isadmin:bool) -> dict:
        """
        Updates the isadmin variable
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            username:str. Should be a username that exists in the database
            password: str. Should match the user in the database
            new_isadmin: str. Should 1 or 0.
            isadmin:bool. True or False. If true allows to give a empty password string
        
        
        """
        check_isadmin = self.check_user_isadmin(new_isadmin) #Checks if new_isadmin is valid
        if(check_isadmin["code"] == 200):
            if (self.data.user_exists(username)):
                if (isadmin): #Bypass password check if an admin is updating the user.
                    user = self.get_user(username=username)
                    user[1]["isadmin"] = True #set isadminn to a boolean
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                if (self.confirm_password(username=username,password=password)): #Checks if the given password matches the one in the db
                    user = self.get_user(username=username)
                    user[1]["isadmin"] = True #set isadminn to a boolean
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    self.data.update_user(username=username,user=user[1])
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Username does not match password"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: User does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            ret_dict = json.dumps(check_isadmin)
            return ret_dict #if new_isadmin is invalid return error 405.



    # --- DELETE USER --- 

    def delete_user(self,username:str,password:str,isadmin:bool) -> dict:
        """
        Delets the given user
        
        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed

        Parameters:
            username:str. Should be a username that exists in the database
            password: str. Should match the user in the database
            isadmin:bool. True or False. If true allows to give a empty password string
        """
        if (isadmin): #Allows admins to delete user without giving a password
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.data.delete_user(username)
            ret_dict = json.dumps(self.status)
            return ret_dict
        if (self.confirm_password(username=username,password=password)): #Checks if the given password matches the one in the db
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.data.delete_user(username)
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Username does not match password"
            ret_dict = json.dumps(self.status)
            return ret_dict

