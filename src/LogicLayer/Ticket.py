import json
from DataLayer.DataAPI import DataAPI
from datetime import datetime, timedelta

class Ticket:
    """ A Class for handling ticket logic."""
    def __init__(self):
        self.code = 405 
        self.message = "None"
        self.status = {"code": self.code, "message": self.message}
        self.data = DataAPI()

    def buy_ticket(self, username:str) -> None:
        """ Buys a ticket for a user. 
        Returns the instance on success.
        
        returns: dict -> {
            code: 200 or 405,
            message: "Success" or "Failed: User does not exist"
        }
        parameters:
            username: str -> username of the user to buy a ticket for
        """
        if self.data.user_exists(username):
            new_ticket_dict = {"owner": username, "type": 1}
            self.data.create_ticket(new_ticket_dict)
            self.status["code"] = 200
            self.status["message"] = "Success"
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: User does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict


    def use_ticket(self, ticket_id, username:str, ) -> None:
        """ Uses a ticket for a user. 
        
        returns: dict -> {
            code: 200 or 405,
            message: "Success" or "Failed: User does not exist"
        }
        parameters:
            username: str -> username of the user using his ticket
            ticket_id: str -> id of the ticket to be used
        """
        if self.data.user_exists(username):
            if self.data.ticket_exists(ticket_id):
                ticket = self.data.get_ticket(ticket_id)
            
                if ticket["owner"] == username:
                    ticket["time_activated"] = datetime.now()
                    self.data.update_ticket(ticket_id, ticket)
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Ticket does not belong to user"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: User does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict

    def gift_ticket(self, ticket_id, username:str, gift_name:str=None) -> None:
        """ Gives a ticket from a user to another user.
        
        returns: dict -> {
            code: 200 or 405,
            message: "Success" or "Failed: Ticket does not belong to user"
        }
        parameters:
            username: str -> username of the user gifting his ticket
            gift_name: str -> username of the user receiving the ticket
            ticket_id: str -> id of the ticket to be gifted
        """

        if self.data.user_exists(username) and self.data.user_exists(gift_name):
            if self.data.ticket_exists(ticket_id):
                ticket = self.data.get_ticket(ticket_id)
                if ticket["owner"] == username:
                    ticket["owner"] = gift_name
                    self.data.update_ticket(ticket_id, ticket)
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Ticket does not belong to user"
            ret_dict = json.dumps(self.status)
            return ret_dict
    

    
    def read_all_tickets(self, username:str) -> None:
        """ Reads all tickets for a user, checks if they are used, unused or expired.

        returns: dict -> {
            code: 200 or 405,
            message: "Success" or "Failed: User does not exist"
            parameters: list -> [
                {
                    "owner": str,
                    "ticket_id": str,
                    "time_activated": datetime,
                    "type": int,
                    "status": str
                }
            ]
        }
        parameters:
            username: str -> username of the user to read tickets for
        """
        
        if self.data.user_exists(username):
            tickets = self.data.get_tickets_by_user(username)
            ticket_results = []
            for ticket in tickets:
                if ticket["time_activated"] is None:
                    status = "unused"
                    time = None
                else:
                    status = "active"
                    time = ticket["time_activated"].strftime("%Y-%m-%d %H:%M")
                    time_diff = timedelta(hours=1)
                    ogtime = ticket["time_activated"]
                    exptime = ogtime + time_diff
                    if exptime < datetime.now():
                        status = "expired"

                ticket_results.append({'owner': ticket["owner"], 'ticket_id': ticket["ticket_id"], 'time_activated': time, 'type': ticket["type"], "status": status})
            

            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = ticket_results
            ret_dict = json.dumps(self.status)
            return ret_dict
            