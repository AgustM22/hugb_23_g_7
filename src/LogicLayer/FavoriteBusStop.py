from DataLayer.DataAPI import DataAPI
import json


class FavoriteBusStop ():
    """ A Class that performs operations for the LogicAPI folder relating to favoriting bus stops."""
    def __init__(self) -> None:
        self.code = 405 #Code = 405 is an error. Code = 200 is a success
        self.message = "None" #Error message
        self.status = {"code":self.code,"message": self.message} #Each check returns a status
        self.data = DataAPI()

    def favorite_bus_stop(self, username:str, bus_stop_name:str, name:str=None) -> str:
        """ Creates a favorite bus stop connection in the data layer.
        If successful, returns the favorite bus stop connection.
        
        username: str (referencing an existing user)
        bus_stop_name: str (referencing an existing bus stop)
        name: str (optional name for connection)
        """

        if self.data.user_exists(username):
            if self.data.bus_stop_exists(bus_stop_name):
                if self.data.favorite_exists(username, bus_stop_name):
                    self.status["code"] = 405
                    self.status["message"] = "Failed: Bus stop has already been favorited by user"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
                else:
                    self.data.favorite_bus_stop(username, bus_stop_name, name)
                    self.status["code"] = 200
                    self.status["message"] = "Success"
                    ret_dict = json.dumps(self.status)
                    return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: Bus stop does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: User does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict
        
    def get_favorite_bus_stops(self, username:str) -> str:
        """ 
        Gets and returns all favorite bus stops from a users username.
        
        username: str (referencing an existing user)
        """
        if self.data.user_exists(username):
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = self.data.get_favorites_from_user(username)
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: User does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict
        
    def unfavorite_bus_stop(self, username:str, bus_stop_name:str) -> str:
        """ Deletes a favorite bus stop connection given its username and bus stop name.
        Returns the instance on success.
        
        username: str (referencing an existing user)
        bus_stop_name: str (referencing an existing bus stop)
        """
        if self.data.user_exists(username):
            if self.data.bus_stop_exists(bus_stop_name):
                thing = self.data.get_favorites_from_user(username)
                self.data.unfavorite_bus_stop(username, bus_stop_name)
                self.status["code"] = 200
                self.status["message"] = "Success"
                ret_dict = json.dumps(self.status)
                return ret_dict
            else:
                self.status["code"] = 405
                self.status["message"] = "Failed: Bus stop does not exist"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: User does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict