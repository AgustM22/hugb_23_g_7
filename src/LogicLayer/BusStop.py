from DataLayer.DataAPI import DataAPI 
import json
class BusStop:
    """ A Class that performs operations for the LogicAPI folder relating to Bus stops."""

    def __init__(self):
        """ Initiates the BusStop class, setting some base variables. """

        self.code = 405 #Code = 405 is an error. Code = 200 is a success
        self.message = "None" #Error message
        self.parameters = "None" #OPTIONAl, If additional data is being send
        self.status = {"code":self.code,"message": self.message,"parameters":self.parameters} #Each check returns a status
        self.data = DataAPI()

    #----------- BUS STOP CHECK CHECKS -----------#

    def check_bus_stop_name(self, name:str):
        """Checks if the given name of a bus stop is a valid name.
        
        Returns:
            status: dict -> {
                code: 200 or 405,
                message: success or failed
            }
        Parameters:
            name: str -> name of the bus stop, should be between 2 and 100 characters.
        """

        if len(name) < 2 or len(name) > 100:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid bus stop name, bus stop name is either too long or too short"
            self.status["parameters"] = "None"
            return self.status
        else:
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = "None"
            return self.status
    
    def check_latitude_longitude(self, latitude:str, longitude:str):
        '''Checks if the given latitude and longitude are valid.
        
        Returns:
            status: dict -> {
                code: 200 or 405,
                message: success or failed

            }
        Parameters:
            latitude: str -> latitude of the bus stop, should be between -90 and 90.
            longitude: str -> longitude of the bus stop, should be between -180 and 180.
        '''
        
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid latitude or longitude"
            self.status["parameters"] = "None"
            return self.status
        
        if abs(latitude) > 90:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid latitude"
            self.status["parameters"] = "None"
            return self.status
        
        elif abs(longitude) > 180:
            self.status["code"] = 405
            self.status["message"] = "Failed: Invalid longitude"
            self.status["parameters"] = "None"
            return self.status
        
    
        self.status["code"] = 200
        self.status["message"] = "Success"
        self.status["parameters"] = "None"
        return self.status
        
    #----------CRUD BUS STOP METHODS----------#

    def create_bus_stop(self, name:str, latitude:str, longitude:str):
        """Checks if given information is a valid for a bus stop, if so it creates a bus stop.
        
        Returns:
            status: dict -> {
                code: 200 or 405,
                message: success or failed
            }
            Parameters:
                name: str -> name of the bus stop, should be between 2 and 100 characters.
        """

        bus_stop_name_check = self.check_bus_stop_name(name)
        if bus_stop_name_check["code"] == 405:
            ret_dict = json.dumps(bus_stop_name_check)
            return ret_dict
        
        latlong_check = self.check_latitude_longitude(latitude, longitude)
        if latlong_check["code"] == 405:
            ret_dict = json.dumps(latlong_check)
            return ret_dict
        
        if self.data.bus_stop_exists(name):
            self.status["code"] = 405
            self.status["message"] = "Error. Bus stop already exists."
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = "None"
            new_bus_stop_dict = {"name": name, "latitude": float(latitude), "longitude": float(longitude)}
            self.data.create_bus_stop(new_bus_stop_dict)
            ret_dict = json.dumps(self.status)
            return ret_dict
        
    def read_all_bus_stops(self):
        """ Gets all bus stops.
        
        returns:
            errordict: dict -> {
                code: 200 or 405,
                message: sucess or failed
                parameters: list({"name":bus_stop_name})
            }

        """
        allbusstops = self.data.get_all_bus_stops()
        self.status["code"]=200
        self.status["message"] =  "Success"
        self.status["parameters"] = allbusstops
        ret_dict = json.dumps(self.status)
        return ret_dict

    def update_bus_stop_name(self, name:str, new_name:str):
        """
        Updates a bus stop with a new name.

        Returns:
            status: dict -> {
                code: 200 or 405,
                message: success or failed
            }
            parameters:
                name: str -> the old name of the bus stop to change.
                new_name: str -> new name of the bus stop, should be between 2 and 100 characters.
            """

        if self.data.bus_stop_exists(name) == True:
            self.status = self.check_bus_stop_name(new_name)

            if self.status["code"] == 405:
                ret_dict = json.dumps(self.status)
                return ret_dict
            else:
                busstop = self.data.get_bus_stop(name)
                busstop["name"] = new_name
                self.data.update_bus_stop(name, busstop)
                self.status["code"] = 200
                self.status["message"] = "Success"
                self.status["parameters"] = "None"
                ret_dict = json.dumps(self.status)
                return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Bus stop does not exist"
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
        
    def update_bus_stop_latitude(self, name:str, latitude:str):
        '''Updates a bus stop with a new latitude.
        
        Returns:
            dict -> {
                code: 200 or 405,
                message: success or failed
            }
            parameters:
                name: str -> the name of the bus stop to change.
                latitude: str -> new latitude of the bus stop, should be between -90 and 90.
        '''
        self.status = self.check_bus_stop_name(name)
        if self.status["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        self.status = self.check_latitude_longitude(latitude, "0")
        if self.status["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        if self.data.bus_stop_exists(name) == True:
            busstop = self.data.get_bus_stop(name)
            busstop["latitude"] = float(latitude)
            self.data.update_bus_stop(name, busstop)
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Bus stop does not exist"
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
        
    def update_bus_stop_longitude(self, name:str, longitude:str):
        '''Updates a bus stop with a new longitude.
        
        Returns:
            dict -> {
                code: 200 or 405,
                message: success or failed
            }
            parameters:
                name: str -> the name of the bus stop to change.
                longitude: str -> new longitude of the bus stop, should be between -180 and 180.
            '''
        self.status = self.check_bus_stop_name(name)
        if self.status["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict

        self.status = self.check_latitude_longitude("0", longitude)
        if self.status["code"] == 405:
            ret_dict = json.dumps(self.status)
            return ret_dict
        if self.data.bus_stop_exists(name) == True:
            busstop = self.data.get_bus_stop(name)
            busstop["longitude"] = float(longitude)
            self.data.update_bus_stop(name, busstop)
            self.status["code"] = 200
            self.status["message"] = "Success"
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Bus stop does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict

    def delete_bus_stop(self, name:str):
        """Deletes a bus stop from the database.
        
        Returns:
            status: dict -> {
                code: 200 or 405,
                message: success or failed
            }
            parameters:
                name: str -> name of the bus stop to delete, must exist.
        """

        if self.data.bus_stop_exists(name) == True:
            self.data.delete_bus_stop(name)
            self.status["code"] = 200
            self.status["message"] = "Success"
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Bus stop does not exist"
            self.status["parameters"] = "None"
            ret_dict = json.dumps(self.status)
            return ret_dict