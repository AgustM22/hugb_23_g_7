from DataLayer.DataAPI import DataAPI
from .User import User
from .BusStop import BusStop
from .Weather import Weather
from .FavoriteBusStop import FavoriteBusStop
from .Route import Route
from .Ticket import Ticket

class LogicAPI(object):
    """ An interface to operations in the logic layer. 
    
    Attributes:
        User : User() --> controls user logic
        data : DataAPI() --> an interface to the database and its operations
        BusStop : BusStop() --> control busStop logic

    Methods:
        // user oriented operations
        create_user(self, name:str, password:str, birthday:str, phone:str, isadmin:str):
        read_all_users(self):
        authorize(self, name:str, password:str) -> dict:
        update_user(self, old_username:str, old_password:str, keyword:str, changed_var:str, isadmin:str) -> dict:
        delete_user(self, username:str, password:str, isadmin:str):

        // bus stop oriented operations
        create_bus_stop(self, name:str):
        read_all_bus_stops(self):
        update_bus_stop(self, oldname:str, newname:str):
        delete_bus_stop(self, name):
    """
    def __init__(self):
        """ Initializing the LogicLayer API. with the following Logic object instances : User(), DataAPI(), and BusStop() """
        self.User = User()
        self.data = DataAPI()
        self.BusStop = BusStop()
        self.Weather = Weather()
        self.FavoriteBusStop = FavoriteBusStop()
        self.Route = Route()
        self.Ticket = Ticket()

    # USER OPERATIONS * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    def create_user(self, name:str, password:str, birthday:str, phone:str, isadmin:str):
        """ Checks whether given information is valid for a user, returns errors if not.

        Parameters:
            name: str. Should be between 2-100 in length
            password: str. Should be 2-100 in length
            birthday: str. Should be formated DD/MM/YYYY
            phone: str. Should be only Numbers AND 7 in length.
            isadmin: str. Should be 1 or 0

        Returns:
            dict["code"] = 200 or 405
            dict["message"] = "Passed" or "Failed.  """

        return self.User.create_user(name, password, birthday, phone, isadmin) #Checking if givven values are correct 


    def read_all_users(self):
        """Gets a list of all users in the system.
        
        Returns:
            tuple[0]["code"] = 200 or 405 (success or fail)
            tuple[0]["message"] = "Passed" or "Failed
            tuple[1] = list of dicts. each dict is a user """

        return self.User.read_all_users()


    def authorize(self, name:str, password:str) -> dict:
        """  Checks if the given user_name and password match and returns string of the user_name and the isadmin variable.
         
        Parameters:
            name : str (key to a user in the system)
            password : str
        
        Returns:
            a status dict with code and message
        """

        return self.User.authorize(name, password)


    def update_user(self, old_username:str, old_password:str, keyword:str, changed_var:str, isadmin:str) -> dict:
        """ Updates a user instance in the system based on the given input.
         
        Parameters:
            old_username : str, the current username of the user to update in the system
            old_password : str, the current password of the user to update
            keyword : str, the field of the user that you wish to update
            changed_var : str, the new field value
            isadmin : str, is an admin performing this operation ("0" or "1" as in true or false)
        
        Returns:
            a status dict with code and message
        """

        if (keyword == "username"):
            return self.User.update_user_username(old_username=old_username, password=old_password, new_username=changed_var, isadmin=isadmin)
        if (keyword == "password"):
            return self.User.update_user_password(username=old_username, old_password=old_password, new_password=changed_var, isadmin=isadmin)
        if (keyword == "phone"):
            return self.User.update_user_phone(username=old_username, password=old_password, new_phone=changed_var, isadmin=isadmin)
        if (keyword == "birthday"):
            return self.User.update_user_birthday(username=old_username, password=old_password, new_birthday=changed_var, isadmin=isadmin)
        if (keyword == "isadmin"):
            return self.User.update_user_isadmin(username=old_username, password=old_password, new_isadmin=changed_var, isadmin=isadmin)


    def delete_user(self, username:str, password:str, isadmin:str):
        """ Deletes a user in the system.
         
        Parameters:
            username : str, referencing an existing user in the system
            password : str, the password of said user
            isadmin : is the operation being performed by an admin. {Bypass password}
        
        Returns 
            a status dict with code and message
        """

        return self.User.delete_user(username, password, isadmin)


    def get_user(self, name: str):
        """
        A function that gets all information of a single user.

        Takes in the variabel "name" which is the username of the user and returns a dicitonary
        of all te information of the user alongside the status of the call.
        """

        return self.User.get_all_user(name)

    # BUS STOP OPERATIONS * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    def create_bus_stop(self, name:str, latitude, longitude):
        """ Checks whether given information is valid for a bus stop, returns errors if not.
         
        Parameters:
            name : str, referencing a bus stop in the system
             
        Returns 
            a status dict with code and message 
        """

        return self.BusStop.create_bus_stop(name, latitude, longitude)


    def read_all_bus_stops(self):
        """ Gets all bus stops that exist in the system.

        Returns:
            Returns a status dict with code and message, and a list of all bus stops.
        """

        return self.BusStop.read_all_bus_stops()


    def update_bus_stop(self, name:str, keyword:str, changed_var:str):
        """ Updates a bus stop with a new name.

        Parameters
            oldname : str, the name of the bus before the change
            newname : str, the name of the bus after the change
        
        Returns:
            A status dict with code and message.
        """

        # Needs to be changed to update each individual variable
        # Keywords: "name", "latitude", "longitude"
        if keyword == "name":
            return self.BusStop.update_bus_stop_name(name, changed_var)
        if keyword == "latitude":
            return self.BusStop.update_bus_stop_latitude(name, changed_var)
        if keyword == "longitude":
            return self.BusStop.update_bus_stop_longitude(name, changed_var)
        
        'return self.BusStop.update_bus_stop()'
    

    def delete_bus_stop(self, name):
        """ Deletes a bus stop from the database.

        Parameters: 
            name : str, referencing an existing bus stop in the system

        Returns 
            A status dict with code and message.
        """

        return self.BusStop.delete_bus_stop(name)
    


    # WEATHER OPERATIONS * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

    def get_current_weather_at_location(self, name:str):
        """
        Gets weather at a given location
        
        Parameters:
            lat : str, latitude
            lon : str, longtitude 
        
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Passed" or "Failed"
            json dict["parameters"] = ??        
        """
        return self.Weather.get_weather(name)
    

    # Favorite bus stop operations * * * * *

    def favorite_bus_stop(self, username:str, bus_stop_name:str, name:str=None):
        """favorite a bus stop for a user
        
        Parameters:
            username : str, referencing a user in the system
            bus_stop_name : str, referencing a bus stop in the system
            name : str, optional name for the favorite bus stop


        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = "None"
        """
        return self.FavoriteBusStop.favorite_bus_stop(username, bus_stop_name, name)
    
    def unfavorite_bus_stop(self, username:str, bus_stop_name:str):
        """unfavorite a bus stop for a user
        
        Parameters:
            username : str, referencing a user in the system
            bus_stop_name : str, referencing a bus stop in the system
            
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = "None" 

        """
        return self.FavoriteBusStop.unfavorite_bus_stop(username, bus_stop_name)
    
    def get_favorite_bus_stops(self, username:str):
        """get a favorite bus stop for a user
        
        Parameters:
            username : str, referencing a user in the system
            
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = list({"BusStopName":bus_stop_name, "name":name})
        """
        return self.FavoriteBusStop.get_favorite_bus_stops(username)
    

    # Route operations * * * * *

    def get_route_duration(self, start_point, end_point):
        """Get the duration of a route between two bus stops
        
        Parameters:
            start_pint : str, Name of starting busstop
            end_point : str, Name of ending busstop
        
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = The duration in minutes     
            """
        return self.Route.get_route_duration(start_point, end_point)
    
    def buy_ticket(self, name):
        """Buy a ticket for a user
        
        Parameters:
            name : str, referencing a user in the system
            
        Returns:
        json dict["code"] = 200 or 405
        json dict["message"] = "Success" or "Failed"
        json dict["parameters"] = "None"
        """
        return self.Ticket.buy_ticket(name)

    def use_ticket(self, name, ticket_id):
        """Use a ticket for a user
        
        Parameters:
            name : str, referencing a user in the system
            ticket_id : str, referencing a ticket in the system
        
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = "None"
        """
        return self.Ticket.use_ticket(ticket_id, name)

    def gift_ticket(self, ticket_id, name, gift_name):
        """Gift a ticket for a user
        
        Parameters:
            ticket_id : str, referencing a ticket in the system
            name : str, referencing a user in the system
            gift_name : str, referencing a user in the system to give the ticket to
        
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = "None"
        """
        return self.Ticket.gift_ticket(ticket_id, name, gift_name)

    def read_all_tickets(self, name):
        """Read all tickets for a user
        
        Parameters:
            name : str, referencing a user in the system
        
        Returns:
            json dict["code"] = 200 or 405
            json dict["message"] = "Success" or "Failed"
            json dict["parameters"] = list({"ticket_id":ticket_id, "owner":owner, "type":type, "time_activated":time_activated})
        """
        return self.Ticket.read_all_tickets(name)