"""----------------------------------------------------------------------------------------
Route : Object
A service provider must have registered their bus stop in google maps for this utility
to work properly. Otherwise it just offers a very rough an estimation of the route.

Depends on the libraries to make HTTP requests to the routes.goole API
- urllib
- json

METHODS 
>
Route : Object
    get_route_duration(start_point:dict, end_point, dict) -> int

----------------------------------------------------------------------------------------"""

import urllib.parse
import urllib.request
import json

from DataLayer.DataAPI import DataAPI

class Route(object):
    """ A utility used for calculating information on routes between known places in the system. """
    APIKEY = 'AIzaSyByZT78UU_dBLiJrFDTHfitU5RWogYcm0g'

    def __init__(self):
        self.code = 405 #Code = 405 is an error. Code = 200 is a success
        self.message = "None" #Error message
        self.status = {"code":self.code,"message": self.message} #Each check returns a status
        self.data = DataAPI()

    def get_route_duration(self, start_point:str, end_point:str) -> int:
        """ Returns the duration in minutes (int) of a route between a starting point and ending point

        start_point, end_point: str
        """

        if self.data.bus_stop_exists(start_point) & self.data.bus_stop_exists(end_point):
            start_busstop = self.data.get_bus_stop(start_point)
            end_busstop = self.data.get_bus_stop(end_point)
        else:
            self.status["code"] = 405
            self.status["message"] = "Failed: Bus stop does not exist"
            ret_dict = json.dumps(self.status)
            return ret_dict

        # Construct request body
        params = {
            "origin": {
                "location":{ 
                      "latLng": {
                            "latitude": start_busstop['latitude'],
                            "longitude": start_busstop['longitude'],
                      }
                }
            },
            "destination": {
                  "location":{ 
                        "latLng": {
                            "latitude": end_busstop['latitude'],
                            "longitude": end_busstop['longitude'],
                        }
                  }
            },
            "travelMode": "TRANSIT",
            "units": "IMPERIAL"
        }
        data = json.dumps(params, indent = 4).encode('utf-8') # Convert to JSON

        # Construct request
        req =  urllib.request.Request("https://routes.googleapis.com/directions/v2:computeRoutes", data)
        req.add_header('X-Goog-Api-Key', Route.APIKEY) # API KEY
        req.add_header('X-Goog-FieldMask', 'routes.duration') # Specify fields to return
        req.add_header('Content-Type', 'application/json') # just for fun

        try:
        # Make request
            with urllib.request.urlopen(req) as response:
                # the API returns duration as a string of 'xxxs', so we must correct for that
                # Example of response : {'routes': [{'duration': '2900s'}]}

                self.status["parameters"] = int(json.loads(response.read().decode('utf-8'))['routes'][0]['duration'][:-1]) // 60
                self.status["code"] = 200
                self.status["message"] = "Success"
                ret_dict = json.dumps(self.status)
                return ret_dict
            
        except:
            self.status["code"] = 405
            self.status["message"] = "Failed: Google maps api Error"
            ret_dict = json.dumps(self.status)
            return ret_dict

    
if __name__ == '__main__':
    ''' Basic debugging utilities'''
    bus_stop1 = {
          "latitude": 64.15430429344381,
          "longitude": -21.999628988514726
    }
    bus_stop2 = {
          "latitude": 64.1268823854544,
          "longitude": -21.87018924031449
    }
    Route().get_route_duration("Hlemmur", "Mjódd")