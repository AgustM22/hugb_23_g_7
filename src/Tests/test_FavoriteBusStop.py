import unittest
import json
from LogicLayer.FavoriteBusStop import FavoriteBusStop


class FavoriteBusStopTest(unittest.TestCase):

    def setUp(self) -> None:
        self.FavoriteBusStop = FavoriteBusStop()
        self.FavoriteBusStop.data.reset()

    def test_favorite_a_bus_stop(self):
        
        #Succsess
        
        #Admin updates busstop
        user = ""
        BusStop = "Hlemmur"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Normal User Updates
        user = "1"
        BusStop = "Hlemmur"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo2")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Normal User Updates same bus stop again.
        user = "1"
        BusStop = "BaBaBOo2"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="123")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        
        #Failed

        #Invalid user
        user = "akjhskdf"
        BusStop = "Hlemmur"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo2")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Invalid bus stop
        user = "1"
        BusStop = "asldkjfæasldfjk"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo2")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Invalid bus stop and name
        user = "10ö9328932825"
        BusStop = "asldkjfæasldfjk"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo2")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])




    def test_unfavorite_a_bus_stop(self):

        #Success
        #Normal User Updates
        user = "Gulli"
        BusStop = "Hlemmur"
        test = self.FavoriteBusStop.favorite_bus_stop(username=user,bus_stop_name=BusStop,name="BaBaBOo2")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])
        user = "Gulli"
        BusStop = "Hlemmur"
        test1 = self.FavoriteBusStop.unfavorite_bus_stop(username=user,bus_stop_name=BusStop)
        test1 = json.loads(test1)
        self.assertEqual(test1["code"],200,msg=test1["message"])

    def test_get_favorite_bus_stop(self):

        #Success

        #User With FavoriteBusStops
        user = "Gulli"
        test = self.FavoriteBusStop.get_favorite_bus_stops(username=user)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])
        self.assertEqual(type(test["parameters"]),list,msg=["message"])
        self.assertEqual(test["parameters"][0]["name"],"Sniðugt",msg=["message"])
        
        #User Without FavoriteBus
        user="Jónatann"
        test = self.FavoriteBusStop.get_favorite_bus_stops(username=user)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])
        self.assertEqual(type(test["parameters"]),list,msg=["message"])
        self.assertEqual(test["parameters"],[],msg=["message"])

        #Failed

        #Non existant user
        user="asdoufpoadfuio"
        test = self.FavoriteBusStop.get_favorite_bus_stops(username=user)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

