import json
import unittest
from LogicLayer.Route import Route

class RouteTests(unittest.TestCase):
    """ A Unit test suite for Routes. """
    

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance and a Route instance. """
        self.route = Route()
        self.route.data.reset()
    

    def test_get_route_duration_exist(self):
        """ Tests getting route duration using Route.get_route_duration. (Should succeed)
        
         Checks that a route duration is returned. """
        test = self.route.get_route_duration("Hlemmur", "Mjódd")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])


    def test_get_route_duration_doesnt_exist(self):
        """ Tests getting route duration for non existing busstops using Route.get_route_duration. (Should fail)
        
         Checks that an error is returned. """
        test = self.route.get_route_duration("Akranes", "Kaplaskjól")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])


    def test_get_route_duration_same_point(self):
        """ Tests getting route duration for same busstops using Route.get_route_duration. (Should fail)
        
         Checks that an error is returned. """
        test = self.route.get_route_duration("Hlemmur", "Hlemmur")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])