""" A file used for unit testing BusstopData. 

Tests by method
>
create_bus_stop(self, columns:dict) {
    def test_create_Busstop(self):
}
get_bus_stop(self, name:str) {
    test_get_Busstop(self):
}
get_all_bus_stops(self) {
    test_get_all_bus_stops(self):
}
update_bus_stop(self, name:str, columns:dict) {
    test_update_Busstop(self)
}
delete_bus_stop(self, name:str) {
    test_delete_Busstop(self)
}
bus_stop_exists(self, name:str) {
    test_Busstop_exists_exists(self):
    test_Busstop_exists_doesnt_exists(self):
}
"""
from DataLayer.busStopData import BusStopData
from DataLayer.DumbDB import DumbDB
import unittest


class BusstopDataTests(unittest.TestCase):
    """ A Unit test suite for BusstopData. """

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance and a BusstopData instance. """
        self.DB = DumbDB()
        self.Busstop_data = BusStopData(self.DB)


    def test_create_Busstop(self):
        """ Tests creating a new Busstop using BusstopData.create_Bus_stop(). (Should succeed)
         
          Checks that the Busstop is created correctly. """
        
        Busstop = {"name": 'Hringbraut'}
        self.Busstop_data.create_bus_stop(Busstop)
        
        self.assertEqual(self.Busstop_data.get_bus_stop(Busstop["name"]), Busstop, "Busstop created incorrectly")


    def test_get_Busstop(self):
        """ Tests getting a Busstop using BusstopData.get_Busstop(name). (should succeed)
         
          Checks if the correct Busstop is returned. """
        Busstop = {"name": 'Laugardalshöll'}
        self.Busstop_data.create_bus_stop(Busstop)

        returned_Busstop = self.Busstop_data.get_bus_stop(Busstop["name"])
        self.assertEqual(Busstop, returned_Busstop, "Incorrect Busstop returned")


    def test_get_all_bus_stops(self):
        """  Tests getting all Busstops using BusstopData.get_all_Busstops(). (Should succeed)
        
         Checks that the Busstops from the population data are returned. """
        
        row1 = {"name": 'Mjódd', "latitude": 64.109701999999999, "longitude": -21.842642999999999}
        row2 = {"name": 'Samgöngustofa', "latitude": 64.136689000000004, "longitude": -21.887231}
        row3 = {"name": 'Grænamýri', "latitude": 64.146191000000002, "longitude": -21.977658999999999}
        row4 = {"name": 'Eiðistorg', "latitude": 64.150386999999995, "longitude": -21.9861}
        row5 = {"name": 'Kaplaskjól', "latitude": 64.144683000000001, "longitude": -21.97099}
        row6 = {"name": 'Hlemmur', "latitude": 64.143253000000001, "longitude": -21.914211000000002}
        
        self.assertEqual(self.Busstop_data.get_all_bus_stops(), [row1, row2, row3, row4, row5, row6], "Incorrect Busstops returned")


    def test_Busstop_exists_exists(self):
        """ Tests checking if a Busstop exists, when it should exist """
        
        self.assertEqual(self.Busstop_data.bus_stop_exists("Mjódd"), True, "Busstop should exist")


    def test_Busstop_exists_doesnt_exists(self):
        """ Tests checking if a Busstop exists, when it shouldn't exist """

        self.assertEqual(self.Busstop_data.bus_stop_exists("Laugardalshöll"), False, "Busstop should not exist")


    def test_update_Busstop(self):
        """ Tests updating a Busstop using BusstopData.update_Bus_stop(). (Should succeed)
         
          Checks that the Busstop is updated correctly. """
        
        Busstop = {"name": 'Hringbraut'}
        self.Busstop_data.update_bus_stop("Mjódd" , Busstop)
        
        self.assertEqual(self.Busstop_data.get_bus_stop(Busstop["name"]), Busstop, "Busstop updated incorrectly")


    def test_delete_Busstop(self):
        """ Tests deleting a Busstop using BusstopData.deletee_Bus_stop(). (Should succeed)
         
          Checks that the Busstop is deleted. """
        
        Busstop = 'Mjódd'
        self.Busstop_data.delete_bus_stop(Busstop)
        
        self.assertEqual(self.Busstop_data.bus_stop_exists(Busstop), False, "Busstop not deleted")