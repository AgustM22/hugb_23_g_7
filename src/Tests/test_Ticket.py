import unittest
import json

from LogicLayer.Ticket import Ticket


class TicketTests(unittest.TestCase):


    def setUp(self) -> None:
        self.ticket = Ticket()
        self.ticket.data.reset()

    def test_buy_ticket(self):
        """Testing buying a ticket"""

        #Valid ticket
        name = "Hemmi"
        test = self.ticket.buy_ticket(name)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid ticket
        name = "a"
        test = self.ticket.buy_ticket(name)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_use_ticket(self):
        """Testing using a ticket"""

        #Valid ticket
        ticket_id = "3"
        username = "Jón G"
        test = self.ticket.use_ticket(ticket_id, username)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid ticket and user
        ticket_id = "a"
        username = "Hafnarfjordur"
        test = self.ticket.use_ticket(ticket_id, username)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
    
    def test_gift_ticket(self):
        """Testing gifting a ticket"""

        #Valid ticket
        ticket_id = "2"
        username = "Hemmi"
        gift_name = "Jón G"
        test = self.ticket.gift_ticket(ticket_id, username, gift_name)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid ticket, user and gift
        ticket_id = "a"
        username = "Hafnarfjordur"
        gift_name = "Hafnarfjordur"
        test = self.ticket.gift_ticket(ticket_id, username, gift_name)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
    