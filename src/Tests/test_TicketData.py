import unittest
from DataLayer.TicketData import TicketData
from DataLayer.DumbDB import DumbDB

class TicketDataTests(unittest.TestCase):
    """ A Unit test suite for TicketData. """

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance and a TicketData instance. """
        self.DB = DumbDB()
        self.ticket_data = TicketData(self.DB)


    def test_get_ticket(self):
        """ Tests getting a ticket using get_ticket()

        Checksthat the ticket returned is the same one as expected """
        ticket = self.ticket_data.get_ticket("3")
        expected_ticket = {'owner': 'Jón G', 'ticket_id': '3', 'time_activated': None, 'type': 1}

        self.assertEqual(ticket, expected_ticket)


    def test_get_all_tickets(self):
        """ Tests getting all tickets using get_all_tickets()
         
        Checks that all populated tickets are returned. """
        all_tickets = self.ticket_data.get_all_tickets()

        row1 = {'owner': 'Gulli', 'ticket_id': '1', 'time_activated': None, 'type': 1}
        row2 = {'owner': 'Hemmi', 'ticket_id': '2', 'time_activated': None, 'type': 1}
        row3 = {'owner': 'Jón G', 'ticket_id': '3', 'time_activated': None, 'type': 1}
        row4 = {'owner': 'Jónatann', 'ticket_id': '4', 'time_activated': None, 'type': 1}

        self.assertEqual(all_tickets, [row1, row2, row3, row4])


    def test_get_tickets_by_user(self):
        """ Tests getting tickets by user name using get_tickets_by_user()
        
        Checks if all tickets for user are returned"""
        tickets = self.ticket_data.get_tickets_by_user("Gulli")

        self.assertEqual(tickets, [{'owner': 'Gulli', 'ticket_id': '1', 'time_activated': None, 'type': 1}])


    def test_create_ticket(self):
        """ Tests creating a ticket using create_ticket()
        
        Checks if ticket is created successfully"""
        self.ticket_data.create_ticket({"owner": "Dagur Maður", "type": 1})
        
        ticket = self.ticket_data.get_tickets_by_user("Dagur Maður")

        self.assertEqual(ticket, [{'owner': 'Dagur Maður', 'ticket_id': '5', 'time_activated': None, 'type': 1}])


    def test_update_ticket(self):
        """Tests updatig a ticket using update_ticket()
        
        Checks if ticket data is correctly updated"""
        ticket = {'owner': 'Jón G', 'ticket_id': "3", 'time_activated': None, 'type': 2}
        self.ticket_data.update_ticket("3", ticket)

        self.assertEqual(self.ticket_data.get_ticket("3"), ticket)


    def test_ticket_exists_exists(self):
        """Tests checking if a ticket exists when it should using ticket_exists()
        
        Checks if correct boolean is returned"""
        self.assertEqual(self.ticket_data.ticket_exists("3"), True)


    def test_ticket_exists_doesnt_exists(self):
        """Tests checking if a ticket exists when it shouldn't using ticket_exists()
        
        Checks if correct boolean is returned"""
        self.assertEqual(self.ticket_data.ticket_exists("5"), False)