""" A file used for unit testing userData. 

Tests by method 
> 
UserData.create_user() {
    test_create_user(): // normal case
}
UserData.get_user() {
    test_get_user(): // normal case
}
UserData.get_all_users()> {
    test_get_all_users(): // normal case
}
UserData.delete_user() {
    test_delete_user() // normal case
}
UserData.update_user() {
    test_update_user() // normal case
}
UserData.user_exists() {
    test_user_exists_exists(): // normal case
    test_user_exists_doesnt_exists(): // *fail* user does not exist
}


"""

from DataLayer.userData import UserData
from DataLayer.DumbDB import DumbDB
from datetime import date
import unittest


class UserDataTests(unittest.TestCase):
    """ A Unit test suite for userData. """

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance and a UserData instance. """
        self.DB = DumbDB()
        self.user_data = UserData(self.DB)


    def test_create_user(self):
        """ Tests creating a new user using UserData.create_user(). (Should succeed)
         
          Checks that the user is created correctly. """
        
        user = {"name": 'Alli', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        self.user_data.create_user(user)
        
        self.assertEqual(self.user_data.get_user(user["name"]), user, "User created incorrectly")


    def test_get_user(self):
        """ Tests getting a user using UserData.get_user(name). (should succeed)
         
          Checks if the correct user is returned. """
        user = {"name": 'Alli', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        self.user_data.create_user(user)

        returned_user = self.user_data.get_user(user["name"])
        self.assertEqual(user, returned_user, "Incorrect user returned")


    def test_get_all_users(self):
        """  Tests getting all users using UserData.get_all_users(). (Should succeed)
        
         Checks that the users from the population data are returned. """
        
        row1 = {"name": 'Jónatann', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}
        row5 = {"name": 'Gusty', "password": 'sunshine', "birthday": date(1990, 9, 14), "phone": '003545812345', "isadmin": False}
        row6 = {"name": 'Gulli', "password": 'letmein', "birthday": date(1995, 8, 25), "phone": '003545642345', "isadmin": False}
        row7 = {"name": 'Alfreð', "password": 'dragon', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": False}
        row8 = {"name": '', "password": '', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": True}
        row9 = {"name": '1', "password": '', "birthday": date(2001, 1, 28), "phone": '003542318745', "isadmin": False}
        
        self.assertEqual(self.user_data.get_all_users(), [row1, row2, row3, row4, row5, row6, row7, row8, row9], "Incorrect users returned")


    def test_user_exists_exists(self):
        """ Tests checking if a user exists, when it should exist """
        
        self.assertEqual(self.user_data.user_exists("Jónatann"), True, "User should exist")


    def test_user_exists_doesnt_exists(self):
        """ Tests checking if a user exists, when it shouldn't exist """

        self.assertEqual(self.user_data.user_exists("Alli"), False, "User should not exist")


    def test_update_user(self):
        """ Tests updating a user using UserData.update_user(). (Should succeed)
         
          Checks that the User is updated correctly. """

        user = {"name": 'Alli', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        self.user_data.update_user("Gusty", user)
        
        self.assertEqual(self.user_data.get_user(user["name"]), user, "User updated incorrectly")


    def test_delete_user(self):
        """ Tests deleting a Busstop using userData.deletee_user(). (Should succeed)
         
          Checks that the user is deleted. """
        
        user = 'Gusty'
        self.user_data.delete_user(user)
        
        self.assertEqual(self.user_data.user_exists(user), False, "user not deleted")