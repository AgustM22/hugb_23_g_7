import unittest
import json
from LogicLayer.BusStop import BusStop

class BusStopTests(unittest.TestCase):

    def setUp(self):
        self.busstop = BusStop()
        self.busstop.data.reset()

    def test_bust_stop_name(self):
        """Testing check_bus_stop_name unit"""

        #Valid name
        name = "Hafnarfjordur"
        test = self.busstop.check_bus_stop_name(name)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid name
        name = "a"
        test = self.busstop.check_bus_stop_name(name)
        self.assertEqual(test["code"],405,msg=test["message"])
        name = "152364712534687213546781523467812152368471278634576125437682547124357125837451278364582176548125347851278457231541267845837165472163451236452153487651287346512763452"
        test = self.busstop.check_bus_stop_name(name)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_create_bus_stop(self):
        """Testing creating a bus stop"""

        #Valid bus stop
        name = "Hafnarfjordur"
        test = self.busstop.create_bus_stop(name,"60","20")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid busstop
        name = "a"
        test = self.busstop.create_bus_stop(name,"1087589234957834","12040924580320989870")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        name = "152364712534687213546781523467812152368471278634576125437682547124357125837451278364582176548125347851278457231541267845837165472163451236452153487651287346512763452"
        test = self.busstop.create_bus_stop(name,"60","10")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        test = self.busstop.create_bus_stop(name,"60182903475023495","1080849237050870")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
            

    def test_update_bus_stop_name(self):
        """Testing updating a bus stop"""

        #Valid bus stop name
        oldname = "Hlemmur"
        newname = "Hlemmur2"
        test = self.busstop.update_bus_stop_name(oldname, newname)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid bus stop names
        newname = "Hafnarfjordur2"
        oldname = "0282384098432"
        test = self.busstop.update_bus_stop_name(oldname, newname)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_update_bus_stop_latitude(self):
        """Testing update bus stop latitude"""

        #Valid latitude
        name = "Hlemmur"
        latitude = "60"
        test = self.busstop.update_bus_stop_latitude(name, latitude)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid latitude
        test = self.busstop.update_bus_stop_latitude(name, "999")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        #Invalid latitude, not float
        test = self.busstop.update_bus_stop_latitude(name, "hallo")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        #Invalid name
        test = self.busstop.update_bus_stop_latitude("13j89hcusd", "60")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_update_bus_stop_longitude(self):
        """Testing update bus stop longitude"""

        #Valid longitude
        name = "Hlemmur"
        longitude = "20"
        test = self.busstop.update_bus_stop_longitude(name, longitude)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid longitude
        test = self.busstop.update_bus_stop_longitude(name, "99999")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        #Invalid longitude, not float
        test = self.busstop.update_bus_stop_longitude(name, "hallo")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        #Invalid name
        test = self.busstop.update_bus_stop_longitude("13j89hcusd", "60")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])


    def test_delete_bus_stop(self):
        """Testing deleting a bus stop"""

        #Valid bus stop
        name = "Mjódd"
        test = self.busstop.delete_bus_stop(name)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid bus stop
        name = "Mjódd"
        test = self.busstop.delete_bus_stop(name)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_get_all_bus_stops(self):
        test = self.busstop.read_all_bus_stops()
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])
        self.assertEqual(type(test["parameters"]),list,msg=test["message"])
        self.assertEqual(type(test["parameters"][0]),dict,msg=test["message"])

