""" A file used for unit testing DumbDB. 


Tests listed by method
>
-- DumbDB -- 
__init__(){

}
create_table() {
    test_create_table():
}
delete_table() name:str){

}
list_tables(){
    test_list_tabless():
}

-- Table --
DumbDB.Table.__init__() {
    test_create_table()
}
DumbDB.Table.get_all_rows() {
    test_get_all_rows_empty():
    test_get_all_rows_multiple_users():
}
DumbDB.Table.get_rows() {
    test_get_rows():
}
DumbDB.Table.get_row() {
    test_get_row_empty_column():
    test_get_row_column():
    test_get_row_column_doesnt_exist():
    test_get_row_doesnt_exist():
}
DumbDB.Table.get_column_names() {
    test_get_column_names()
}
DumbDB.Table.key_exists() {
    test_key_exists_true()
    test_key_exists_false_populated_table()
    test_key_exists_false_empty_table()
}
DumbDB.Table.create_row() {
    test_create_row_all_fields():
    test_create_row_missing_fields():  
    test_create_row_no_key_field():
}
DumbDB.Table.delete_row() {
    test_delete_row__row_exists()
    test_delete_row__row_not_exists()
}
DumbDB.Table.update_row() {
    test_update_row__row_exists_key   // updating key
    test_update_row__row_exists
    test_update_row__row_exists_wrong_parameter_type
    test_update_row__row_not_exists
}
"""
from DataLayer.DumbDB import DumbDB
from datetime import date
import unittest



class DumbDBTests(unittest.TestCase):
    """ A Unit test suite for DumbDB, the simulated database for our software. """

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance with a nested table instance to test with. """

        self.db = DumbDB()

        # Create tables
        self.db.create_table("user", {"name":str(), "password":str(), "birthday":date(2002,12,31), "phone":str(), "isadmin":bool()}, "name")

        
    def test_create_table(self):
        """ Tests creating a table in the database simulation using DumbDB.create_table(). (Should succeed)
        (also uses DumbDB.Table.columns, DumbDB.Table.key_column, and DumbDB.Table.rows)

        Checks that columns match what was created, that the key_column is correct, and that the table starts out as empty. """

        self.db.create_table("busstops", {"name":str(), "password":str(), "birthday":date(2002,12,31), "phone":str(), "isadmin":bool()}, "name")

        self.assertEqual(self.db.tables["busstops"].columns, {"name":str(), "password":str(), "birthday":date(2002,12,31), "phone":str(), "isadmin":bool()}, "Table columns incorrect")
        self.assertEqual(self.db.tables["busstops"].key_column, "name", "Incorrect key column")
        self.assertEqual(self.db.tables["busstops"].rows, dict(), "Rows not empty")


    def test_list_tabless(self):
        """ Tests getting a list of tables from a database instance using DumbDB.list_tables(). (Should succeed)
        (also uses DumbDB.create_table())
        
        Checks that the correct (existing) tables are listed. """

        self.db.create_table("busstops", {"name":str(), "password":str(), "birthday":date(2002,12,31), "phone":str(), "isadmin":bool()}, "name")
        
        output = self.db.list_tables()
        self.assertEqual(type(output), list)
        self.assertEqual(output, ["user", "busstops"])


    def test_create_row_all_fields(self):
        """ Tests creating a row using DumbDB.Table.create_row(), where all fields are fully filled out for a given table. (Should succeed)
        (retrieves a row from the database in order to assert correctness).
         
        Checks that the row is stored correctly in the table. """
        
        row = {"name": 'Jónatann', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        self.db.tables["user"].create_row(row)
        
        self.assertEqual(self.db.tables["user"].rows["Jónatann"], row, "Table row dictionary incorrect")


    def test_create_row_missing_fields(self):
        """ Tests creating a row using DumbDB.Table.create_row(), where the fields are only partially filled in. (Should succeed)
        
        Checks that the row is stored correctly in the table. """

        row = {"name": 'Jónatann', "birthday": date(2002, 5, 2), "isadmin": False}
        row_result = {"name": 'Jónatann', "password": None, "birthday": date(2002, 5, 2), "phone": None, "isadmin": False}

        self.db.tables["user"].create_row(row)
        
        self.assertEqual(self.db.tables["user"].rows["Jónatann"], row_result, "Table row dictionary incorrect")
    

    def test_create_row_no_key_field(self):
        """ Tests using DumbDB.Table.create_row() to create a row where the key-field is not specified. (Should fail)
        
        Checks that the appropriate exception is raised. """

        row = {"password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        with self.assertRaises(Exception):
             self.db.tables["user"].create_row(row)
           

    def test_get_all_rows_empty(self):
        """ Tests getting all rows from an empty table using DumbDB.Table.get_all_rows(). (should succeed)
         
        Checks that the return is an empty list. """

        self.assertEqual(self.db.tables["user"].get_all_rows(), [])


    def test_get_all_rows_multiple_users(self):
        """ Tests that DumbDB.Table.get_all_rows() for a non-empty table.
        (also uses Table.create_row())
         
        Checks that the return of get_all_rows() is correct. """

        row1 = {"name": 'Jónatann', "password": 'J0nniN3tt1', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)
        
        self.assertEqual(self.db.tables["user"].get_all_rows(), [row1, row2, row3, row4])


    def test_get_rows(self):
        """ Tests DumbDB.Table.get_rows() for a query that should return multiple rows.
        (also uses DumbDB.Table.create_row())
        
        Checks that the return from get_rows is correct and as expected. """

        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        self.assertEqual(self.db.tables["user"].get_rows("123456", "password"), [row1, row2])

 
    def test_get_row_empty_column(self):
        """ Tests DumbDB.Table.get_row() with no specified column parameter. (should succeed (return assume key-field))
        (also uses DumbDB.Table.create_row())
        
        Checks that the return from DumbDB.Table.get_row() is as expected. """

        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        self.assertEqual(self.db.tables["user"].get_row(row4['name']), row4)

 
    def test_get_row_column(self):
        """ Tests DumbDB.Table.get_row() with all columns explicitly given. (should succeed)
        (also uses DumbDB.Table.create_row())
        (uses a date object as a search value parameter)
        
        Checks that the return from DumbDB.Table.get_row() is as expected. """

        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        self.assertEqual(self.db.tables["user"].get_row(row3['birthday'], "birthday"), row3)

 
    def test_get_row_column_doesnt_exist(self):
        """ Tests DumbDB.Table.get_row() for value that does not exist. (should succeed, return None)
        (also uses DumbDB.Table.create_row())
        (uses a date object as a search value parameter)
        
        Checks that the return from DumbDB.Table.get_row() is as expected (None). """
        
        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        test_date = date(2002, 12, 23)

        self.assertEqual(self.db.tables["user"].get_row(test_date, "birthday"), None)

 
    def test_get_row_doesnt_exist(self):
        """ Tests DumbDB.Table.get_row() with all columns explicitly given when a matching row doesn't exist. (should succeed)
        (also uses DumbDB.Table.create_row())
        
        Checks that the return from DumbDB.Table.get_row() is as expected (None). """

        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)
        
        # self.assertEqual(self.db.tables["user"].get_row("Alfreð"), None) #TODO UNCOMMENT and fix

 
    def test_get_column_names(self):
        """ Tests getting the column names for a specific table using DumbDB.Table.get_column_names(). (Should succeed).

        Checks that the return of DumbDB.Table.get_column_names() is as expected. """

        self.assertEqual(self.db.tables["user"].get_column_names(), ["name", "password", "birthday", "phone", "isadmin"])


    def test_key_exists__true(self):
        """ Tests DumbDB.Tables.key_exists() with a key that exists. (should succeed)

        Checks that the return is True. """

        key = "Jónatann"
        row1 = {"name": key, "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        self.db.tables["user"].create_row(row1)

        self.assertEqual(self.db.tables["user"].key_exists(key), True)


    def test_key_exists__false_populated_table(self):
        """ Tests DumbDB.Tables.key_exists() with a key that does not exists in a populated table. (should succeed)

        Checks that the return is True. """

        key = "Sjonni3"
        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        self.assertEqual(self.db.tables["user"].key_exists(key), False)


    def test_key_exists__false_empty_table(self):
        """ Tests DumbDB.Tables.key_exists() with a key that does not exists in an empty table. (should succeed)

        Checks that the return is False. """

        key = "Sjonni3"
        self.assertEqual(self.db.tables["user"].key_exists(key), False)


    def test_delete_row__row_exists(self):
        """ Tests DumbDB.Tables.delete_row() with a key to a row that exists (should succeed)

        Checks delete row returns the deleted row
        Checks that the row is actually removed. """

        key = 'Jónatann'
        row1 = {"name": key, "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        self.assertEqual(self.db.tables["user"].delete_row(key), row1)
        self.assertCountEqual([row2, row3, row4], self.db.tables["user"].get_all_rows())


    def test_delete_row__row_not_exists(self):
        """ Tests DumbDB.Tables.delete_row() with a key that does not exist. (should fail)

        Checks delete row fails, when the key does not exist
        Checks that no changes have been made to the table. """

        key = 'Sjonni3'
        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        # self.assertEqual(self.db.tables["user"].delete_row(key), row1)
        with self.assertRaises(Exception):
             self.db.tables["user"].delete_row(key)

        self.assertCountEqual([row1, row2, row3, row4], self.db.tables["user"].get_all_rows())


    def test_update_row__row_exists_key(self):
        """ Tests DumbDB.Tables.update_row() with a key to a row that exists, and specifically updates the key in the row (should succeed)

        Checks that update returns the new row.
        Checks that all rows are as they should be in the table.
        Checks that the row can be retrieved using the new key.
          """
        key = 'Sjonni3'
        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)
    
        new_row = row1
        new_row['name'] = key

        self.assertEqual(self.db.tables["user"].update_row('Jónatann', new_row), row1)

        self.assertCountEqual([new_row, row2, row3, row4], self.db.tables["user"].get_all_rows())
        self.assertEqual(self.db.tables["user"].get_row(key), new_row)



    def test_update_row__row_exists(self):
        """ Tests DumbDB.Tables.delete_row() with a key to a row that exists (should succeed)

        Checks that the table is as it should be, before the update.
        Checks that update returns the new row.
        Checks that the table is as it should be, after the update. """
        key = 'Sjonni3'
        row1 = {"name": key, "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        new_row = row1
        new_row["password"] = "49987993"

        self.assertCountEqual([row1, row2, row3, row4], self.db.tables["user"].get_all_rows())
        self.assertEqual(self.db.tables["user"].update_row(key, new_row), new_row)
        self.assertCountEqual([new_row, row2, row3, row4], self.db.tables["user"].get_all_rows())


    def test_update_row__row_exists_wrong_parameter_type(self):
        """ Tests DumbDB.Tables.delete_row() with a key to a row that exists, but has a wrong type for some parameter (should fail)

        Checks that update raisese the appropriate exception. 
        Checks that no changes have been made to the table"""

        key = 'Jónatann'
        row1 = {"name": key, "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        new_row = row1
        new_row["password"] = 49987993

        with self.assertRaises(Exception):
             self.db.tables["user"].update_row(key, new_row)

        self.assertCountEqual([row1, row2, row3, row4], self.db.tables["user"].get_all_rows())


    def test_update_row__row_not_exists(self):
        """ Tests DumbDB.Tables.delete_row() with a key to a row that does not exist. (should fail)

        Checks that update raisese the appropriate exception. 
        Checks that no changes have been made to the table"""
        key = 'Sjonni3'
        row1 = {"name": 'Jónatann', "password": '123456', "birthday": date(2002, 5, 2), "phone": '003545812345', "isadmin": False}
        row2 = {"name": 'Dagur Maður', "password": '123456', "birthday": date(1890, 3, 7), "phone": '003545123345', "isadmin": False}
        row3 = {"name": 'Hemmi', "password": 'qwerty', "birthday": date(2005, 12, 23), "phone": '00354576545', "isadmin": False}
        row4 = {"name": 'Jón G', "password": 'iloveyou', "birthday": date(1975, 2, 15), "phone": '003545563145', "isadmin": False}

        self.db.tables["user"].create_row(row1)
        self.db.tables["user"].create_row(row2)
        self.db.tables["user"].create_row(row3)
        self.db.tables["user"].create_row(row4)

        new_row = row1
        new_row["name"] = key

        with self.assertRaises(Exception):
            self.db.tables["user"].update_row(key, new_row)

        self.assertCountEqual([row1, row2, row3, row4], self.db.tables["user"].get_all_rows())
