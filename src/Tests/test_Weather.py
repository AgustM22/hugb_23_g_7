import unittest
import json
from LogicLayer.Weather import Weather

class WeatherTests(unittest.TestCase):
    
    
    def setUp(self) -> None:
        self.Weather = Weather()

    def test_get_weather_at_location(self):
        test = self.Weather.get_weather("Hlemmur")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        test = self.Weather.get_weather("GagaGugu")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        test = self.Weather.get_weather("123,123")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

