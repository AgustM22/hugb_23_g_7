import datetime
import unittest
import json

from LogicLayer.User import User


class UserTests(unittest.TestCase):
    #IMPORTANT when writing a test function. the function name must start with test_(Name of test)


    def setUp(self) -> None:
        self.User = User()


    # -- Validate Tests --

    def test_username(self):
        """Unit test for a users username"""
        #Valid Username
        name = "asdasda"
        test = self.User.check_user_username(name)
        self.assertEqual(test["code"],200,msg=test["message"])
        #Username too short
        name = "2"
        test = self.User.check_user_username(name)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_passwrod(self):
        """Unit test for a users password"""
        
        #Valid password
        password = "Password123"
        test = self.User.check_user_password(password)
        self.assertEqual(test["code"],200,msg=test["message"])
        
        #Too short of a password
        password = "pas"
        test = self.User.check_user_password(password)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Password with no numbers
        password = "SuperLongPasswordWithNoNumbers"
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_birthday(self):

        #Valid birthday
        birthday = "31/03/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid Day
        birthday = "-3/03/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "0/03/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "123/03/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "AB/03/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Invalid Month
        birthday = "31/-3/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/0/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/132/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/AB/2001"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Invalid Year
        birthday = "31/03/-3"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/03/1"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/03/12342134"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])
        birthday = "31/03/asdasd"
        test = self.User.check_user_birthday(birthday)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_phone(self):

        #Valid phone number
        phone = "1231234"
        test = self.User.check_user_phone(phone)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid phone number
        phone = "123"
        test = self.User.check_user_phone(phone)
        self.assertEqual(test["code"],405,msg=test["message"])
        phone = "123456789"
        test = self.User.check_user_phone(phone)
        self.assertEqual(test["code"],405,msg=test["message"])
        phone = "ABCD"
        test = self.User.check_user_phone(phone)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_isadmin(self):
        
        #Valid isadmin
        isadmin = "0"
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],200,msg=test["message"])
        isadmin = "1"
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Invalid isadmin
        isadmin = "testing"
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],405,msg=test["message"])
        isadmin = 3.3
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Mabye this should work.. ?
        isadmin = 0
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],405,msg=test["message"])
        isadmin = 1
        test = self.User.check_user_isadmin(isadmin)
        self.assertEqual(test["code"],405,msg=test["message"])

    # ---- Read users ----

    def test_get_all_users(self):
        test = self.User.read_all_users()
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])
        self.assertEqual(type(test["parameters"]),list)
    
    #---- Update testes -----


    def test_update_user_username(self):

        #Succsess
        test = self.User.get_user("Gulli")
        test = self.User.update_user_username(old_username="Gulli",password="letmein",new_username="Gulli2",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])


        #Too short of a new name
        test = self.User.update_user_username(old_username="Gulli2",password="letmein",new_username="a",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Wrong password
        test = test = self.User.update_user_username(old_username="Gulli2",password="let",new_username="Gulli3",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

    def test_update_user_password(self):

        #Success
        test = self.User.get_user("Hemmi")
        test = self.User.update_user_password(username="Hemmi",old_password="qwerty",new_password="qwerty123",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Too short of a new password
        test = self.User.update_user_password(username="Hemmi",old_password="qwerty123",new_password="a",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        self.assertEqual(test["message"],"Failed: Invalid password, given password was too short or too long.",msg=test["message"])

        #Wrong password
        test = self.User.update_user_password(username="Hemmi",old_password="let",new_password="qwerty123",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        self.assertEqual(test["message"],"Failed: Username does not match password",msg=test["message"])
        
    def test_update_user_phone(self):
        #Success
        test = self.User.get_user("Jónatann")
        test = self.User.update_user_phone(username="Jónatann",password="J0nniN3tt1",new_phone="5812345",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Too short of a new phone
        test = self.User.update_user_phone(username="Jónatann",password="J0nniN3tt1",new_phone="a",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        self.assertEqual(test["message"],"Failed: Invalid phone number, given string was either wrong size or not a number.",msg=test["message"])

        #Wrong password
        test = self.User.update_user_phone(username="Jónatann",password="J0nniN3tt221",new_phone="5812345",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        self.assertEqual(test["message"],"Failed: Username does not match password",msg=test["message"])
        

    
    def test_update_user_birthday(self):
        #Success
        test = self.User.get_user("Gusty")
        test = self.User.update_user_birthday(username="Gusty",password="sunshine",new_birthday="31/03/2001",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        #Too short of a new birthday
        test = self.User.update_user_birthday(username="Gusty",password="sunshine",new_birthday="a",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        #Wrong password
        test = self.User.update_user_birthday(username="Gusty",password="J0nniNsdasds3tt221",new_birthday="31/03/2001",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
        self.assertEqual(test["message"],"Failed: Username does not match password",msg=test["message"])
        


    # --- Delete User Tests ---

    def test_delete_user(self):
        
        #Passed
        testuser = self.User.get_user("Jón G")
        test = self.User.delete_user(testuser[1]["name"],testuser[1]["password"],isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        test = self.User.delete_user("asdasd","123",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])

        test = self.User.delete_user("Alfreð",password="password123",isadmin=False)
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])


    def test_autherize_user(self):
        test = self.User.get_user("")
        test = self.User.authorize("","")
        test = json.loads(test)
        self.assertEqual(test["code"],200,msg=test["message"])

        test = self.User.authorize("","123123")
        test = json.loads(test)
        self.assertEqual(test["code"],405,msg=test["message"])
