import unittest
from DataLayer.FavoriteBusStopData import FavoriteBusStopData
from DataLayer.DumbDB import DumbDB
from DataLayer.busStopData import BusStopData
from DataLayer.userData import UserData


class FavoriteBusStopDataTests(unittest.TestCase):
    """ A Unit test suite for Favorite busstop. """

    def setUp(self):
        """ Sets up the test suite.
        Creates a database instance and a FavoriteBusStop instance. """
        self.DB = DumbDB()
        self.Busstop_data = BusStopData(self.DB)
        self.User_data = UserData(self.DB)
        self.FavoriteBusStop_data = FavoriteBusStopData(self.DB, self.User_data, self.Busstop_data)

    
    def test_get_favorites_from_user(self):
        """ Tests getting all favorite busstops from user using FavoriteBusStopdata.get_favorites_from_user. (Should succeed)
        
         Checks that the favorite busstops from the population data are returned. """
        test = self.FavoriteBusStop_data.get_favorites_from_user("Hemmi")

        row1 = {"bus_stop": 'Mjódd', "name": 'Helvíti'}
        row2 = {"bus_stop": 'Samgöngustofa', "name": None}
        
        self.assertEqual(test, [row1, row2], "Incorrect favorites returned")


    def test_make_favorite(self):
        """  Tests getting making a favorite busstop for a user using FavoriteBusStopdata.make_favorite. (Should succeed)
        
         Checks that the favorite busstop is favorited. """
        self.FavoriteBusStop_data.make_favorite("Gulli", "Eiðistorg", "Nice")

        test = self.FavoriteBusStop_data.get_favorites_from_user("Gulli")

        row1 = {"bus_stop": 'Mjódd', "name": 'Sniðugt'}
        row2 = {"bus_stop": 'Eiðistorg', "name": 'Nice'}
        
        self.assertEqual(test, [row1, row2], "Busstop not favorited")


    def test_unfavorite(self):
        """  Tests unfavoriting a busstop for a user using FavoriteBusStopdata.unfavorite. (Should succeed)
        
         Checks that the favorite busstop is unfavorited. """
        self.FavoriteBusStop_data.unfavorite("Hemmi", "Samgöngustofa")        

        test = self.FavoriteBusStop_data.get_favorites_from_user("Hemmi")

        row1 = {"bus_stop": 'Mjódd', "name": 'Helvíti'}
        
        self.assertEqual(test, [row1], "Incorrect unfavorited")
