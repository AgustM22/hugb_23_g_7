# Klip Klop (Clap-IT)

The best app for commuting

## About

This readme goes over the basics of how to run the project.
If you want to learn more of what this project is about then i reccomend checking out Project overview, found under sprin-1-docs


## Getting started

This project relies on gRPC. if you do not have gRPC installed on your device then you will run into errors.
To install gRPC begin first by updating pip:

> python -m pip install --upgrade pip

Now to install gRPC run the following commands

> python -m pip install grpcio

> python -m pip install grpcio-tools

Now you should be all set to run the server and client.
If you have run into any trouble getting gRPC installed then check this instalation guide out [Here](https://grpc.io/docs/languages/python/quickstart/)

To run the server you must run the following command from the src file 
> py Server.py 

Then once the server is running, in a seperate command line run the following command from the src file, to run the client against the server.
> py UIlayer\UIAPI.py

Note: While we have been developing the project we created two main accounts that can be used to see what the system is capable of. The first being an admin account with a blank username and password, thus you can easily log in and use the admin side, additionally we also created a simple user account that has a username of "1" and a blank password. We reccomend using these accounts if you simply want to take a brief look at the project.

There can be some instances where the UIAPI file will not run and will give an error relating to not being able to import Tkinter. The reason for this is due to Tkinter not being installed natively on your devoce
To fix this, you can run this command and the frontend should work. 
> brew install python-tk

For generating a **coverage report** you must first pip install coverage using
> python -m pip install coverage

After that, you can simply run the following commands from the src file.
> coverage run -m unittest discover Tests

> coverage html

And an HTML file will be created with the report

For running a specific **Unit test** simply run the following command from the src file
> python -m unittest Tests.(Test you want to run)




